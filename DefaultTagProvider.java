/*  Copyright (c) 2006-2007, Vladimir Nikic
    All rights reserved.

    Redistribution and use of this software in source and binary forms,
    with or without modification, are permitted provided that the following
    conditions are met:

    * Redistributions of source code must retain the above
      copyright notice, this list of conditions and the
      following disclaimer.

    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the
      following disclaimer in the documentation and/or other
      materials provided with the distribution.

    * The name of HtmlCleaner may not be used to endorse or promote
      products derived from this software without specific prior
      written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    You can contact Vladimir Nikic by sending e-mail to
    nikic_vladimir@yahoo.com. Please include the word "HtmlCleaner" in the
    subject line.
*/
/**
 * This class is automatically created from ConfigFileTagProvider which reads
 * default XML configuration file with tag descriptions.
 * It is used as default tag info provider.
 * Class is created for performance purposes - parsing XML file requires some
 * processing time.
 */
package org.htmlcleaner;
import org.htmlcleaner.*;
import java.util.HashMap;
public class DefaultTagProvider extends java.util.HashMap implements org.htmlcleaner.ITagInfoProvider {
// singleton instance, used if no other TagInfoProvider is specified
    private static org.htmlcleaner.DefaultTagProvider _instance;
/**
     * @return Singleton instance of this class.
     */
    public static synchronized org.htmlcleaner.DefaultTagProvider getInstance() {
        if (_instance == null) {
            _instance = new org.htmlcleaner.DefaultTagProvider();
        }
        return _instance;
    }
    public DefaultTagProvider() {
        super();
        org.htmlcleaner.TagInfo tagInfo;
        tagInfo = new org.htmlcleaner.TagInfo("div", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("div",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("span", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("span",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("meta", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.HEAD, false, false, false);
        this.put("meta",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("link", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.HEAD, false, false, false);
        this.put("link",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("title", org.htmlcleaner.TagInfo.CONTENT_TEXT, org.htmlcleaner.TagInfo.HEAD, false, true, false);
        this.put("title",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("style", org.htmlcleaner.TagInfo.CONTENT_TEXT, org.htmlcleaner.TagInfo.HEAD, false, false, false);
        this.put("style",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("bgsound", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.HEAD, false, false, false);
        this.put("bgsound",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("h1", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("h1",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("h2", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("h2",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("h3", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("h3",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("h4", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("h4",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("h5", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("h5",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("h6", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("h6",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("p", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("p",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("strong", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("strong",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("em", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("em",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("abbr", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("abbr",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("acronym", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("acronym",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("address", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("address",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("bdo", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("bdo",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("blockquote", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("blockquote",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("cite", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("cite",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("q", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("q",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("code", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("code",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("ins", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("ins",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("del", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("del",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("dfn", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("dfn",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("kbd", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("kbd",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("pre", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("pre",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("samp", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("samp",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("listing", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("listing",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("var", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("var",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("br", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("br",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("wbr", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("wbr",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("nobr", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeTags("nobr");
        this.put("nobr",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("xmp", org.htmlcleaner.TagInfo.CONTENT_TEXT, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("xmp",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("a", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeTags("a");
        this.put("a",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("base", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.HEAD, false, false, false);
        this.put("base",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("img", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("img",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("area", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineFatalTags("map");
        tagInfo.defineCloseBeforeTags("area");
        this.put("area",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("map", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeTags("map");
        this.put("map",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("object", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("object",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("param", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("param",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("applet", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, true, false, false);
        this.put("applet",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("xml", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("xml",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("ul", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("ul",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("ol", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("ol",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("li", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("li,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("li",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("dl", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("dl",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("dt", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeTags("dt,dd");
        this.put("dt",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("dd", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeTags("dt,dd");
        this.put("dd",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("menu", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, true, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("menu",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("dir", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, true, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("dir",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("table", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineAllowedChildrenTags("tr,tbody,thead,tfoot,colgroup,col,form,caption,tr");
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("tr,thead,tbody,tfoot,caption,colgroup,table,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param");
        this.put("table",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("tr", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineFatalTags("table");
        tagInfo.defineRequiredEnclosingTags("tbody");
        tagInfo.defineAllowedChildrenTags("td,th");
        tagInfo.defineHigherLevelTags("thead,tfoot");
        tagInfo.defineCloseBeforeTags("tr,td,th,caption,colgroup");
        this.put("tr",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("td", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineFatalTags("table");
        tagInfo.defineRequiredEnclosingTags("tr");
        tagInfo.defineCloseBeforeTags("td,th,caption,colgroup");
        this.put("td",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("th", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineFatalTags("table");
        tagInfo.defineRequiredEnclosingTags("tr");
        tagInfo.defineCloseBeforeTags("td,th,caption,colgroup");
        this.put("th",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("tbody", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineFatalTags("table");
        tagInfo.defineAllowedChildrenTags("tr,form");
        tagInfo.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        this.put("tbody",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("thead", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineFatalTags("table");
        tagInfo.defineAllowedChildrenTags("tr,form");
        tagInfo.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        this.put("thead",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("tfoot", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineFatalTags("table");
        tagInfo.defineAllowedChildrenTags("tr,form");
        tagInfo.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        this.put("tfoot",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("col", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineFatalTags("table");
        this.put("col",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("colgroup", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineFatalTags("table");
        tagInfo.defineAllowedChildrenTags("col");
        tagInfo.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        this.put("colgroup",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("caption", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineFatalTags("table");
        tagInfo.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        this.put("caption",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("form", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, true);
        tagInfo.defineForbiddenTags("form");
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("option,optgroup,textarea,select,fieldset,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("form",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("input", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeTags("select,optgroup,option");
        this.put("input",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("textarea", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeTags("select,optgroup,option");
        this.put("textarea",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("select", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, true);
        tagInfo.defineAllowedChildrenTags("option,optgroup");
        tagInfo.defineCloseBeforeTags("option,optgroup,select");
        this.put("select",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("option", org.htmlcleaner.TagInfo.CONTENT_TEXT, org.htmlcleaner.TagInfo.BODY, false, false, true);
        tagInfo.defineFatalTags("select");
        tagInfo.defineCloseBeforeTags("option");
        this.put("option",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("optgroup", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, true);
        tagInfo.defineFatalTags("select");
        tagInfo.defineAllowedChildrenTags("option");
        tagInfo.defineCloseBeforeTags("optgroup");
        this.put("optgroup",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("button", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeTags("select,optgroup,option");
        this.put("button",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("label", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("label",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("fieldset", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("fieldset",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("legend", org.htmlcleaner.TagInfo.CONTENT_TEXT, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineRequiredEnclosingTags("fieldset");
        tagInfo.defineCloseBeforeTags("legend");
        this.put("legend",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("isindex", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, true, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("isindex",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("script", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.HEAD_AND_BODY, false, false, false);
        this.put("script",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("noscript", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.HEAD_AND_BODY, false, false, false);
        this.put("noscript",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("b", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("u,i,tt,sub,sup,big,small,strike,blink,s");
        this.put("b",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("i", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("b,u,tt,sub,sup,big,small,strike,blink,s");
        this.put("i",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("u", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, true, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("b,i,tt,sub,sup,big,small,strike,blink,s");
        this.put("u",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("tt", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("b,u,i,sub,sup,big,small,strike,blink,s");
        this.put("tt",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("sub", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("b,u,i,tt,sup,big,small,strike,blink,s");
        this.put("sub",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("sup", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,big,small,strike,blink,s");
        this.put("sup",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("big", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,small,strike,blink,s");
        this.put("big",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("small", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,strike,blink,s");
        this.put("small",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("strike", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, true, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,blink,s");
        this.put("strike",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("blink", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,strike,s");
        this.put("blink",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("marquee", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("marquee",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("s", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, true, false, false);
        tagInfo.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,strike,blink");
        this.put("s",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("hr", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("hr",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("font", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, true, false, false);
        this.put("font",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("basefont", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, true, false, false);
        this.put("basefont",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("center", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, true, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("center",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("comment", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("comment",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("server", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("server",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("iframe", org.htmlcleaner.TagInfo.CONTENT_ALL, org.htmlcleaner.TagInfo.BODY, false, false, false);
        this.put("iframe",tagInfo);
        tagInfo = new org.htmlcleaner.TagInfo("embed", org.htmlcleaner.TagInfo.CONTENT_NONE, org.htmlcleaner.TagInfo.BODY, false, false, false);
        tagInfo.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        this.put("embed",tagInfo);
    }
    public org.htmlcleaner.TagInfo getTagInfo(java.lang.String tagName) {
        return this.get(tagName);
    }
/**
     * Removes tag info with specified name.
     * @param tagName Name of the tag to be removed from the tag provider.
     */
    public void removeTagInfo(java.lang.String tagName) {
        if (tagName != null) {
            this.remove(tagName.toLowerCase());
        }
    }
/**
     * Sets new tag info.
     * @param tagInfo tag info to be added to the provider.
     */
    public void addTagInfo(org.htmlcleaner.TagInfo tagInfo) {
        if (tagInfo != null) {
            this.put(tagInfo.getName().toLowerCase(),tagInfo);
        }
    }
}
