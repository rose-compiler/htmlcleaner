/*  Copyright (c) 2006-2007, Vladimir Nikic
    All rights reserved.
	
    Redistribution and use of this software in source and binary forms, 
    with or without modification, are permitted provided that the following 
    conditions are met:
	
    * Redistributions of source code must retain the above
      copyright notice, this list of conditions and the
      following disclaimer.
	
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the
      following disclaimer in the documentation and/or other
      materials provided with the distribution.
	
    * The name of HtmlCleaner may not be used to endorse or promote 
      products derived from this software without specific prior
      written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.
	
    You can contact Vladimir Nikic by sending e-mail to
    nikic_vladimir@yahoo.com. Please include the word "HtmlCleaner" in the
    subject line.
*/
/**
 * Main HTML tokenizer.
 * <p>It's task is to parse HTML and produce list of valid tokens:
 * open tag tokens, end tag tokens, contents (text) and comments.
 * As soon as new item is added to token list, cleaner is invoked
 * to clean current list at the end.</p>
 */
package org.htmlcleaner;
import org.htmlcleaner.*;
import java.io.*;
import java.util.*;
abstract public class HtmlTokenizer extends java.lang.Object {
    final private static int WORKING_BUFFER_SIZE = 1024;
    private java.io.BufferedReader _reader;
    private char[] _working = new char[WORKING_BUFFER_SIZE];
    private transient int _pos = 0;
    private transient int _len =  -1;
    private transient char[] _saved = new char[512];
    private transient int _savedLen = 0;
    private transient org.htmlcleaner.DoctypeToken _docType = null;
    private transient org.htmlcleaner.TagToken _currentTagToken = null;
    private transient java.util.List<org.htmlcleaner.BaseToken> _tokenList = new java.util.ArrayList<org.htmlcleaner.BaseToken>();
    private boolean _asExpected = true;
    private boolean _isScriptContext = false;
    private org.htmlcleaner.CleanerProperties props;
    private boolean isOmitUnknownTags;
    private boolean isTreatUnknownTagsAsContent;
    private boolean isOmitDeprecatedTags;
    private boolean isTreatDeprecatedTagsAsContent;
    private boolean isNamespacesAware;
    private boolean isOmitComments;
    private boolean isAllowMultiWordAttributes;
    private boolean isAllowHtmlInsideAttributes;
    private org.htmlcleaner.CleanerTransformations transformations;
    private org.htmlcleaner.ITagInfoProvider tagInfoProvider;
    private java.lang.StringBuilder commonStr = new java.lang.StringBuilder();
/**
     * Constructor - cretes instance of the parser with specified content.
     * 
     * @param reader
     * @param props
     * @param transformations
     * @param tagInfoProvider
     * 
     * @throws IOException
     */
    public HtmlTokenizer(java.io.Reader reader, org.htmlcleaner.CleanerProperties props, org.htmlcleaner.CleanerTransformations transformations, org.htmlcleaner.ITagInfoProvider tagInfoProvider) throws java.io.IOException {
        super();
        this._reader = new java.io.BufferedReader(reader);
        this.props = props;
        this.isOmitUnknownTags = props.isOmitUnknownTags();
        this.isTreatUnknownTagsAsContent = props.isTreatUnknownTagsAsContent();
        this.isOmitDeprecatedTags = props.isOmitDeprecatedTags();
        this.isTreatDeprecatedTagsAsContent = props.isTreatDeprecatedTagsAsContent();
        this.isNamespacesAware = props.isNamespacesAware();
        this.isOmitComments = props.isOmitComments();
        this.isAllowMultiWordAttributes = props.isAllowMultiWordAttributes();
        this.isAllowHtmlInsideAttributes = props.isAllowHtmlInsideAttributes();
        this.transformations = transformations;
        this.tagInfoProvider = tagInfoProvider;
    }
    private void addToken(org.htmlcleaner.BaseToken token) {
        _tokenList.add(token);
        this.makeTree(_tokenList);
    }
    abstract void makeTree(java.util.List<org.htmlcleaner.BaseToken> tokenList);
    abstract org.htmlcleaner.TagNode createTagNode(java.lang.String name);
    private void readIfNeeded(int neededChars) throws java.io.IOException {
        if (_len ==  -1 && _pos + neededChars >= WORKING_BUFFER_SIZE) {
            int numToCopy = WORKING_BUFFER_SIZE - _pos;
            java.lang.System.arraycopy(_working,_pos,_working,0,numToCopy);
            _pos = 0;
            int expected = WORKING_BUFFER_SIZE - numToCopy;
            int size = 0;
            int charsRead;
            int offset = numToCopy;
            do {
                charsRead = _reader.read(_working,offset,expected);
                if (charsRead >= 0) {
                    size += charsRead;
                    offset += charsRead;
                    expected -= charsRead;
                }
            }
            while (charsRead >= 0 && expected > 0);
            if (expected > 0) {
                _len = size + numToCopy;
            }
            for (int i = 0; i < (_len >= 0 ? _len : WORKING_BUFFER_SIZE); i++) {
                int ch = _working[i];
                if (ch >= 1 && ch <= 32 && ch != 10 && ch != 13) {
                    _working[i] = '\u0020';
                }
            }
        }
    }
    java.util.List<org.htmlcleaner.BaseToken> getTokenList() {
        return this._tokenList;
    }
    private void go() throws java.io.IOException {
        _pos++;
        this.readIfNeeded(0);
    }
    private void go(int step) throws java.io.IOException {
        _pos += step;
        this.readIfNeeded(step - 1);
    }
/**
     * Checks if content starts with specified value at the current position.
     * @param value
     * @return true if starts with specified value, false otherwise.
     * @throws IOException
     */
    private boolean startsWith(java.lang.String value) throws java.io.IOException {
        int valueLen = value.length();
        this.readIfNeeded(valueLen);
        if (_len >= 0 && _pos + valueLen > _len) {
            return false;
        }
        for (int i = 0; i < valueLen; i++) {
            char ch1 = java.lang.Character.toLowerCase(value.charAt(i));
            char ch2 = java.lang.Character.toLowerCase(_working[_pos + i]);
            if (ch1 != ch2) {
                return false;
            }
        }
        return true;
    }
    private boolean startsWithSimple(java.lang.String value) throws java.io.IOException {
        int valueLen = value.length();
        this.readIfNeeded(valueLen);
        if (_len >= 0 && _pos + valueLen > _len) {
            return false;
        }
        for (int i = 0; i < valueLen; i++) {
            if (value.charAt(i) != _working[_pos + i]) {
                return false;
            }
        }
        return true;
    }
/**
     * Checks if character at specified position is whitespace.
     * @param position
     * @return true is whitespace, false otherwise.
     */
    private boolean isWhitespace(int position) {
        if (_len >= 0 && position >= _len) {
            return false;
        }
        return java.lang.Character.isWhitespace(_working[position]);
    }
/**
     * Checks if character at current runtime position is whitespace.
     * @return true is whitespace, false otherwise.
     */
    private boolean isWhitespace() {
        return this.isWhitespace(_pos);
    }
    private boolean isWhitespaceSafe() {
        return java.lang.Character.isWhitespace(_working[_pos]);
    }
/**
     * Checks if character at specified position is equal to specified char.
     * @param position
     * @param ch
     * @return true is equals, false otherwise.
     */
    private boolean isChar(int position, char ch) {
        if (_len >= 0 && position >= _len) {
            return false;
        }
        return java.lang.Character.toLowerCase(ch) == java.lang.Character.toLowerCase(_working[position]);
    }
/**
     * Checks if character at current runtime position is equal to specified char.
     * @param ch
     * @return true is equal, false otherwise.
     */
    private boolean isChar(char ch) {
        return this.isChar(_pos,ch);
    }
    private boolean isCharSimple(char ch) {
        return (_len < 0 || _pos < _len) && (ch == _working[_pos]);
    }
/**
     * @return Current character to be read, but first it must be checked if it exists.
     * This method is made for performance reasons to be used instead of isChar(...).
     */
    private char getCurrentChar() {
        return _working[_pos];
    }
    private boolean isCharEquals(char ch) {
        return _working[_pos] == ch;
    }
/**
     * Checks if character at specified position can be identifier start.
     * @param position
     * @return true is may be identifier start, false otherwise.
     */
    private boolean isIdentifierStartChar(int position) {
        if (_len >= 0 && position >= _len) {
            return false;
        }
        char ch = _working[position];
        return java.lang.Character.isUnicodeIdentifierStart(ch) || ch == '\u005f';
    }
/**
     * Checks if character at current runtime position can be identifier start.
     * @return true is may be identifier start, false otherwise.
     */
    private boolean isIdentifierStartChar() {
        return this.isIdentifierStartChar(_pos);
    }
/**
     * Checks if character at current runtime position can be identifier part.
     * @return true is may be identifier part, false otherwise.
     */
    private boolean isIdentifierChar() {
        if (_len >= 0 && _pos >= _len) {
            return false;
        }
        char ch = _working[_pos];
        return java.lang.Character.isUnicodeIdentifierStart(ch) || java.lang.Character.isDigit(ch) || org.htmlcleaner.Utils.isIdentifierHelperChar(ch);
    }
    private boolean isValidXmlChar() {
        return this.isAllRead() || org.htmlcleaner.Utils.isValidXmlChar(_working[_pos]);
    }
    private boolean isValidXmlCharSafe() {
        return org.htmlcleaner.Utils.isValidXmlChar(_working[_pos]);
    }
/**
     * Checks if end of the content is reached.
     */
    private boolean isAllRead() {
        return _len >= 0 && _pos >= _len;
    }
/**
     * Saves specified character to the temporary buffer.
     * @param ch
     */
    private void save(char ch) {
        if (_savedLen >= _saved.length) {
            char[] newSaved = new char[_saved.length + 512];
            java.lang.System.arraycopy(_saved,0,newSaved,0,_saved.length);
            _saved = newSaved;
        }
        _saved[_savedLen++] = ch;
    }
/**
     * Saves character at current runtime position to the temporary buffer.
     */
    private void saveCurrent() {
        if ( !this.isAllRead()) {
            this.save(_working[_pos]);
        }
    }
    private void saveCurrentSafe() {
        this.save(_working[_pos]);
    }
/**
     * Saves specified number of characters at current runtime position to the temporary buffer.
     * @throws IOException
     */
    private void saveCurrent(int size) throws java.io.IOException {
        this.readIfNeeded(size);
        int pos = _pos;
        while ( !this.isAllRead() && (size > 0)) {
            this.save(_working[pos]);
            pos++;
            size--;
        }
    }
/**
     * Skips whitespaces at current position and moves foreward until
     * non-whitespace character is found or the end of content is reached.
     * @throws IOException
     */
    private void skipWhitespaces() throws java.io.IOException {
        while ( !this.isAllRead() && this.isWhitespaceSafe()) {
            this.saveCurrentSafe();
            this.go();
        }
    }
    private boolean addSavedAsContent() {
        if (_savedLen > 0) {
            this.addToken(new org.htmlcleaner.ContentNode(_saved, _savedLen));
            _savedLen = 0;
            return true;
        }
        return false;
    }
/**
     * Starts parsing HTML.
     * @throws IOException
     */
    void start() throws java.io.IOException {
        _currentTagToken = null;
        _tokenList.clear();
        _asExpected = true;
        _isScriptContext = false;
// initialize runtime values
        boolean isLateForDoctype = false;
        this._pos = WORKING_BUFFER_SIZE;
        this.readIfNeeded(0);
        boolean isScriptEmpty = true;
        while ( !this.isAllRead()) {
            _savedLen = 0;
            _currentTagToken = null;
            _asExpected = true;
            this.readIfNeeded(10);
// resets all the runtime values
// this is enough for making decision
            if (_isScriptContext) {
                if (this.startsWith("</script") && (this.isWhitespace(_pos + 8) || this.isChar(_pos + 8,'\u003e'))) {
                    this.tagEnd();
                }
                else if (isScriptEmpty && this.startsWithSimple("<!--")) {
                    this.comment();
                }
                else {
                    boolean isTokenAdded = this.content();
                    if (isScriptEmpty && isTokenAdded) {
                        final org.htmlcleaner.BaseToken lastToken = _tokenList.get(_tokenList.size() - 1);
                        if (lastToken != null) {
                            final java.lang.String lastTokenAsString = lastToken.toString();
                            if (lastTokenAsString != null && lastTokenAsString.trim().length() > 0) {
                                isScriptEmpty = false;
                            }
                        }
                    }
                }
                if ( !_isScriptContext) {
                    isScriptEmpty = true;
                }
            }
            else {
                if (this.startsWith("<!doctype")) {
                    if ( !isLateForDoctype) {
                        this.doctype();
                        isLateForDoctype = true;
                    }
                    else {
                        this.ignoreUntil('\u003c');
                    }
                }
                else if (this.startsWithSimple("</") && this.isIdentifierStartChar(_pos + 2)) {
                    isLateForDoctype = true;
                    this.tagEnd();
                }
                else if (this.startsWithSimple("<!--")) {
                    this.comment();
                }
                else if (this.startsWithSimple("<") && this.isIdentifierStartChar(_pos + 1)) {
                    isLateForDoctype = true;
                    this.tagStart();
                }
                else if (props.isIgnoreQuestAndExclam() && (this.startsWithSimple("<!") || this.startsWithSimple("<?"))) {
                    this.ignoreUntil('\u003e');
                    if (this.isCharSimple('\u003e')) {
                        this.go();
                    }
                }
                else {
                    this.content();
                }
            }
        }
        _reader.close();
    }
/**
     * Checks if specified tag name is one of the reserved tags: HTML, HEAD or BODY
     * @param tagName
     * @return
     */
    private boolean isReservedTag(java.lang.String tagName) {
        tagName = tagName.toLowerCase();
        return "html".equals(tagName) || "head".equals(tagName) || "body".equals(tagName);
    }
/**
     * Parses start of the tag.
     * It expects that current position is at the "<" after which
     * the tag's name follows.
     * @throws IOException
     */
    private void tagStart() throws java.io.IOException {
        this.saveCurrent();
        this.go();
        if (this.isAllRead()) {
            return;
        }
        java.lang.String tagName = this.identifier();
        org.htmlcleaner.TagTransformation tagTransformation = null;
        if (transformations != null && transformations.hasTransformationForTag(tagName)) {
            tagTransformation = transformations.getTransformation(tagName);
            if (tagTransformation != null) {
                tagName = tagTransformation.getDestTag();
            }
        }
        if (tagName != null) {
            org.htmlcleaner.TagInfo tagInfo = tagInfoProvider.getTagInfo(tagName);
            if ((tagInfo == null &&  !isOmitUnknownTags && isTreatUnknownTagsAsContent &&  !this.isReservedTag(tagName)) || (tagInfo != null && tagInfo.isDeprecated() &&  !isOmitDeprecatedTags && isTreatDeprecatedTagsAsContent)) {
                this.content();
                return;
            }
        }
        org.htmlcleaner.TagNode tagNode = this.createTagNode(tagName);
        _currentTagToken = tagNode;
        if (_asExpected) {
            this.skipWhitespaces();
            this.tagAttributes();
            if (tagName != null) {
                if (tagTransformation != null) {
                    tagNode.transformAttributes(tagTransformation);
                }
                this.addToken(_currentTagToken);
            }
            if (this.isCharSimple('\u003e')) {
                this.go();
                if ("script".equalsIgnoreCase(tagName)) {
                    _isScriptContext = true;
                }
            }
            else if (this.startsWithSimple("/>")) {
                this.go(2);
                if ("script".equalsIgnoreCase(tagName)) {
                    this.addToken(new org.htmlcleaner.EndTagToken(tagName));
                }
            }
            _currentTagToken = null;
        }
        else {
            this.addSavedAsContent();
        }
    }
/**
     * Parses end of the tag.
     * It expects that current position is at the "<" after which
     * "/" and the tag's name follows.
     * @throws IOException
     */
    private void tagEnd() throws java.io.IOException {
        this.saveCurrent(2);
        this.go(2);
        if (this.isAllRead()) {
            return;
        }
        java.lang.String tagName = this.identifier();
        if (transformations != null && transformations.hasTransformationForTag(tagName)) {
            org.htmlcleaner.TagTransformation tagTransformation = transformations.getTransformation(tagName);
            if (tagTransformation != null) {
                tagName = tagTransformation.getDestTag();
            }
        }
        if (tagName != null) {
            org.htmlcleaner.TagInfo tagInfo = tagInfoProvider.getTagInfo(tagName);
            if ((tagInfo == null &&  !isOmitUnknownTags && isTreatUnknownTagsAsContent &&  !this.isReservedTag(tagName)) || (tagInfo != null && tagInfo.isDeprecated() &&  !isOmitDeprecatedTags && isTreatDeprecatedTagsAsContent)) {
                this.content();
                return;
            }
        }
        _currentTagToken = new org.htmlcleaner.EndTagToken(tagName);
        if (_asExpected) {
            this.skipWhitespaces();
            this.tagAttributes();
            if (tagName != null) {
                this.addToken(_currentTagToken);
            }
            if (this.isCharSimple('\u003e')) {
                this.go();
            }
            if ("script".equalsIgnoreCase(tagName)) {
                _isScriptContext = false;
            }
            _currentTagToken = null;
        }
        else {
            this.addSavedAsContent();
        }
    }
/**
     * Parses an identifier from the current position.
     * @throws IOException
     */
    private java.lang.String identifier() throws java.io.IOException {
        _asExpected = true;
        if ( !this.isIdentifierStartChar()) {
            _asExpected = false;
            return null;
        }
        commonStr.delete(0,commonStr.length());
        while ( !this.isAllRead() && this.isIdentifierChar()) {
            this.saveCurrentSafe();
            commonStr.append(_working[_pos]);
            this.go();
        }
// strip invalid characters from the end
        while (commonStr.length() > 0 && org.htmlcleaner.Utils.isIdentifierHelperChar(commonStr.charAt(commonStr.length() - 1))) {
            commonStr.deleteCharAt(commonStr.length() - 1);
        }
        if (commonStr.length() == 0) {
            return null;
        }
        java.lang.String id = commonStr.toString();
        int columnIndex = id.indexOf('\u003a');
        if (columnIndex >= 0) {
            java.lang.String prefix = id.substring(0,columnIndex);
            java.lang.String suffix = id.substring(columnIndex + 1);
            int nextColumnIndex = suffix.indexOf('\u003a');
            if (nextColumnIndex >= 0) {
                suffix = suffix.substring(0,nextColumnIndex);
            }
            id = isNamespacesAware ? (prefix + ":" + suffix) : suffix;
        }
        return id;
    }
/**
     * Parses list tag attributes from the current position.
     * @throws IOException
     */
    private void tagAttributes() throws java.io.IOException {
        while ( !this.isAllRead() && _asExpected &&  !this.isCharSimple('\u003e') &&  !this.startsWithSimple("/>")) {
            this.skipWhitespaces();
            java.lang.String attName = this.identifier();
            if ( !_asExpected) {
                if ( !this.isCharSimple('\u003c') &&  !this.isCharSimple('\u003e') &&  !this.startsWithSimple("/>")) {
                    if (this.isValidXmlChar()) {
                        this.saveCurrent();
                    }
                    this.go();
                }
                if ( !this.isCharSimple('\u003c')) {
                    _asExpected = true;
                }
                continue;
            }
            java.lang.String attValue;
            this.skipWhitespaces();
            if (this.isCharSimple('\u003d')) {
                this.saveCurrentSafe();
                this.go();
                attValue = this.attributeValue();
            }
            else if (org.htmlcleaner.CleanerProperties.BOOL_ATT_EMPTY.equals(props.booleanAttributeValues)) {
                attValue = "";
            }
            else if (org.htmlcleaner.CleanerProperties.BOOL_ATT_TRUE.equals(props.booleanAttributeValues)) {
                attValue = "true";
            }
            else {
                attValue = attName;
            }
            if (_asExpected) {
                _currentTagToken.setAttribute(attName,attValue);
            }
        }
    }
/**
     * Parses a single tag attribute - it is expected to be in one of the forms:
     * 		name=value
     * 		name="value"
     * 		name='value'
     * 		name
     * @throws IOException
     */
    private java.lang.String attributeValue() throws java.io.IOException {
        this.skipWhitespaces();
        if (this.isCharSimple('\u003c') || this.isCharSimple('\u003e') || this.startsWithSimple("/>")) {
            return "";
        }
        boolean isQuoteMode = false;
        boolean isAposMode = false;
        commonStr.delete(0,commonStr.length());
        if (this.isCharSimple('\'')) {
            isAposMode = true;
            this.saveCurrentSafe();
            this.go();
        }
        else if (this.isCharSimple('"')) {
            isQuoteMode = true;
            this.saveCurrentSafe();
            this.go();
        }
        while ( !this.isAllRead() && (((isAposMode &&  !this.isCharEquals('\'') || isQuoteMode &&  !this.isCharEquals('"')) && (isAllowHtmlInsideAttributes ||  !this.isCharEquals('\u003e') &&  !this.isCharEquals('\u003c')) && (isAllowMultiWordAttributes ||  !this.isWhitespaceSafe())) || ( !isAposMode &&  !isQuoteMode &&  !this.isWhitespaceSafe() &&  !this.isCharEquals('\u003e') &&  !this.isCharEquals('\u003c')))) {
            if (this.isValidXmlCharSafe()) {
                commonStr.append(_working[_pos]);
                this.saveCurrentSafe();
            }
            this.go();
        }
        if (this.isCharSimple('\'') && isAposMode) {
            this.saveCurrentSafe();
            this.go();
        }
        else if (this.isCharSimple('"') && isQuoteMode) {
            this.saveCurrentSafe();
            this.go();
        }
        return commonStr.toString();
    }
    private boolean content() throws java.io.IOException {
        while ( !this.isAllRead()) {
            if (this.isValidXmlCharSafe()) {
                this.saveCurrentSafe();
            }
            this.go();
            if (this.isCharSimple('\u003c')) {
                break;
            }
        }
        return this.addSavedAsContent();
    }
    private void ignoreUntil(char ch) throws java.io.IOException {
        while ( !this.isAllRead()) {
            this.go();
            if (this.isChar(ch)) {
                break;
            }
        }
    }
    private void comment() throws java.io.IOException {
        this.go(4);
        while ( !this.isAllRead() &&  !this.startsWithSimple("-->")) {
            if (this.isValidXmlCharSafe()) {
                this.saveCurrentSafe();
            }
            this.go();
        }
        if (this.startsWithSimple("-->")) {
            this.go(3);
        }
        if (_savedLen > 0) {
            if ( !isOmitComments) {
                java.lang.String hyphenRepl = props.getHyphenReplacementInComment();
                java.lang.String comment = new java.lang.String(_saved, 0, _savedLen).replaceAll("--",hyphenRepl + hyphenRepl);
                if (comment.length() > 0 && comment.charAt(0) == '\u002d') {
                    comment = hyphenRepl + comment.substring(1);
                }
                int len = comment.length();
                if (len > 0 && comment.charAt(len - 1) == '\u002d') {
                    comment = comment.substring(0,len - 1) + hyphenRepl;
                }
                this.addToken(new org.htmlcleaner.CommentNode(comment));
            }
            _savedLen = 0;
        }
    }
    private void doctype() throws java.io.IOException {
        this.go(9);
        this.skipWhitespaces();
        java.lang.String part1 = this.identifier();
        this.skipWhitespaces();
        java.lang.String part2 = this.identifier();
        this.skipWhitespaces();
        java.lang.String part3 = this.attributeValue();
        this.skipWhitespaces();
        java.lang.String part4 = this.attributeValue();
        this.ignoreUntil('\u003c');
        _docType = new org.htmlcleaner.DoctypeToken(part1, part2, part3, part4);
    }
    public org.htmlcleaner.DoctypeToken getDocType() {
        return _docType;
    }
}
