/*  Copyright (c) 2006-2007, Vladimir Nikic
    All rights reserved.
	
    Redistribution and use of this software in source and binary forms, 
    with or without modification, are permitted provided that the following 
    conditions are met:
	
    * Redistributions of source code must retain the above
      copyright notice, this list of conditions and the
      following disclaimer.
	
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the
      following disclaimer in the documentation and/or other
      materials provided with the distribution.
	
    * The name of HtmlCleaner may not be used to endorse or promote 
      products derived from this software without specific prior
      written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.
	
    You can contact Vladimir Nikic by sending e-mail to
    nikic_vladimir@yahoo.com. Please include the word "HtmlCleaner" in the
    subject line.
*/
/**
 * <p>
 * Class contains information about single HTML tag.<br/>
 * It also contains rules for tag balancing. For each tag, list of dependant
 * tags may be defined. There are several kinds of dependancies used to reorder
 * tags:
 * <ul>
 *      <li>
 * 		  fatal tags - required outer tag - the tag will be ignored during
 *        parsing (will be skipped) if this fatal tag is missing. For example, most web
 *        browsers ignore elements TD, TR, TBODY if they are not in the context of TABLE tag.
 *      </li>
 *      <li>
 *        required enclosing tags - if there is no such, it is implicitely
 *        created. For example if TD is out of TR - open TR is created before.
 *      </li>
 *      <li>
 *        forbidden tags - it is not allowed to occure inside - for example
 *        FORM cannot be inside other FORM and it will be ignored during cleanup.
 *      </li>
 *      <li>
 *        allowed children tags - for example TR allowes TD and TH. If there
 *        are some dependant allowed tags defined then cleaner ignores other tags, treating
 *        them as unallowed, unless they are in some other relationship with this tag.
 *      </li>
 *      <li>
 *        higher level tags - for example for TR higher tags are THEAD, TBODY, TFOOT.
 *      </li>
 *      <li>
 *        tags that must be closed and copied - for example, in
 *        <code>&lt;a href="#"&gt;&lt;div&gt;....</code> tag A must be closed before DIV but
 *        copied again inside DIV.
 *      </li>
 *      <li>
 *        tags that must be closed before closing this tag and copied again after -
 *        for example, in <code>&lt;i&gt;&lt;b&gt;at&lt;/i&gt; first&lt;/b&gt; text </code>
 *        tag B must be closed before closing I, but it must be copied again after resulting
 *        finally in sequence: <code>&lt;i&gt;&lt;b&gt;at&lt;/b&gt;&lt;/i&gt;&lt;b&gt; first&lt;/b&gt; text </code>.
 *      </li>
 * </ul>
 * </p>
 * 
 * <p>
 * Tag TR for instance (table row) may define the following dependancies:
 *      <ul>
 *          <li>fatal tag is <code>table</code></li>
 *          <li>required enclosing tag is <code>tbody</code></li>
 *          <li>allowed children tags are <code>td,th</code></li>
 *          <li>higher level tags are <code>thead,tfoot</code></li>
 *          <li>tags that muste be closed before are <code>tr,td,th,caption,colgroup</code></li>
 *      </ul>
 * meaning the following: <br>
 *   <ul>
 *      <li><code>tr</code> must be in context of <code>table</code>, otherwise it will be ignored,</li>
 *      <li><code>tr</code> may can be directly inside <code>tbody</code>, <code>tfoot</code> and <code>thead</code>,
 *          otherwise <code>tbody</code> will be implicitely created in front of it.</li>
 *      <li><code>tr</code> can contain <code>td</code> and <code>th</code>, all other tags and content will be pushed out of current
 *      limiting context, in the case of html tables, in front of enclosing <code>table</code> tag.</li>
 *      <li>if previous open tag is one of <code>tr</code>, <code>caption</code> or <code>colgroup</code>, it will be implicitely closed.</li>
 *   </ul>
 * </p>
 */
package org.htmlcleaner;
import org.htmlcleaner.*;
import java.util.*;
public class TagInfo extends java.lang.Object {
    final protected static int HEAD_AND_BODY = 0;
    final protected static int HEAD = 1;
    final protected static int BODY = 2;
    final protected static int CONTENT_ALL = 0;
    final protected static int CONTENT_NONE = 1;
    final protected static int CONTENT_TEXT = 2;
    private java.lang.String name;
    private int contentType;
    private java.util.Set mustCloseTags = new java.util.HashSet();
    private java.util.Set higherTags = new java.util.HashSet();
    private java.util.Set childTags = new java.util.HashSet();
    private java.util.Set permittedTags = new java.util.HashSet();
    private java.util.Set copyTags = new java.util.HashSet();
    private java.util.Set continueAfterTags = new java.util.HashSet();
    private int belongsTo = BODY;
    private java.lang.String requiredParent = null;
    private java.lang.String fatalTag = null;
    private boolean deprecated = false;
    private boolean unique = false;
    private boolean ignorePermitted = false;
    public TagInfo(java.lang.String name, int contentType, int belongsTo, boolean depricated, boolean unique, boolean ignorePermitted) {
        super();
        this.name = name;
        this.contentType = contentType;
        this.belongsTo = belongsTo;
        this.deprecated = depricated;
        this.unique = unique;
        this.ignorePermitted = ignorePermitted;
    }
    public void defineFatalTags(java.lang.String commaSeparatedListOfTags) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            java.lang.String currTag = tokenizer.nextToken();
            this.fatalTag = currTag;
            this.higherTags.add(currTag);
        }
    }
    public void defineRequiredEnclosingTags(java.lang.String commaSeparatedListOfTags) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            java.lang.String currTag = tokenizer.nextToken();
            this.requiredParent = currTag;
            this.higherTags.add(currTag);
        }
    }
    public void defineForbiddenTags(java.lang.String commaSeparatedListOfTags) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            java.lang.String currTag = tokenizer.nextToken();
            this.permittedTags.add(currTag);
        }
    }
    public void defineAllowedChildrenTags(java.lang.String commaSeparatedListOfTags) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            java.lang.String currTag = tokenizer.nextToken();
            this.childTags.add(currTag);
        }
    }
    public void defineHigherLevelTags(java.lang.String commaSeparatedListOfTags) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            java.lang.String currTag = tokenizer.nextToken();
            this.higherTags.add(currTag);
        }
    }
    public void defineCloseBeforeCopyInsideTags(java.lang.String commaSeparatedListOfTags) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            java.lang.String currTag = tokenizer.nextToken();
            this.copyTags.add(currTag);
            this.mustCloseTags.add(currTag);
        }
    }
    public void defineCloseInsideCopyAfterTags(java.lang.String commaSeparatedListOfTags) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            java.lang.String currTag = tokenizer.nextToken();
            this.continueAfterTags.add(currTag);
        }
    }
    public void defineCloseBeforeTags(java.lang.String commaSeparatedListOfTags) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            java.lang.String currTag = tokenizer.nextToken();
            this.mustCloseTags.add(currTag);
        }
    }
// getters and setters
    public java.lang.String getName() {
        return name;
    }
    public void setName(java.lang.String name) {
        this.name = name;
    }
    public int getContentType() {
        return contentType;
    }
    public java.util.Set getMustCloseTags() {
        return mustCloseTags;
    }
    public void setMustCloseTags(java.util.Set mustCloseTags) {
        this.mustCloseTags = mustCloseTags;
    }
    public java.util.Set getHigherTags() {
        return higherTags;
    }
    public void setHigherTags(java.util.Set higherTags) {
        this.higherTags = higherTags;
    }
    public java.util.Set getChildTags() {
        return childTags;
    }
    public void setChildTags(java.util.Set childTags) {
        this.childTags = childTags;
    }
    public java.util.Set getPermittedTags() {
        return permittedTags;
    }
    public void setPermittedTags(java.util.Set permittedTags) {
        this.permittedTags = permittedTags;
    }
    public java.util.Set getCopyTags() {
        return copyTags;
    }
    public void setCopyTags(java.util.Set copyTags) {
        this.copyTags = copyTags;
    }
    public java.util.Set getContinueAfterTags() {
        return continueAfterTags;
    }
    public void setContinueAfterTags(java.util.Set continueAfterTags) {
        this.continueAfterTags = continueAfterTags;
    }
    public java.lang.String getRequiredParent() {
        return requiredParent;
    }
    public void setRequiredParent(java.lang.String requiredParent) {
        this.requiredParent = requiredParent;
    }
    public int getBelongsTo() {
        return belongsTo;
    }
    public void setBelongsTo(int belongsTo) {
        this.belongsTo = belongsTo;
    }
    public java.lang.String getFatalTag() {
        return fatalTag;
    }
    public void setFatalTag(java.lang.String fatalTag) {
        this.fatalTag = fatalTag;
    }
    public boolean isDeprecated() {
        return deprecated;
    }
    public void setDeprecated(boolean deprecated) {
        this.deprecated = deprecated;
    }
    public boolean isUnique() {
        return unique;
    }
    public void setUnique(boolean unique) {
        this.unique = unique;
    }
    public boolean isIgnorePermitted() {
        return ignorePermitted;
    }
    public boolean isEmptyTag() {
        return CONTENT_NONE == contentType;
    }
    public void setIgnorePermitted(boolean ignorePermitted) {
        this.ignorePermitted = ignorePermitted;
    }
// other functionality
    boolean allowsBody() {
        return CONTENT_NONE != contentType;
    }
    boolean isHigher(java.lang.String tagName) {
        return higherTags.contains(tagName);
    }
    boolean isCopy(java.lang.String tagName) {
        return copyTags.contains(tagName);
    }
    boolean hasCopyTags() {
        return  !copyTags.isEmpty();
    }
    boolean isContinueAfter(java.lang.String tagName) {
        return continueAfterTags.contains(tagName);
    }
    boolean hasPermittedTags() {
        return  !permittedTags.isEmpty();
    }
    boolean isHeadTag() {
        return belongsTo == HEAD;
    }
    boolean isHeadAndBodyTag() {
        return belongsTo == HEAD || belongsTo == HEAD_AND_BODY;
    }
    boolean isMustCloseTag(org.htmlcleaner.TagInfo tagInfo) {
        if (tagInfo != null) {
            return mustCloseTags.contains(tagInfo.getName()) || tagInfo.contentType == CONTENT_TEXT;
        }
        return false;
    }
    boolean allowsItem(org.htmlcleaner.BaseToken token) {
        if (contentType != CONTENT_NONE && token instanceof org.htmlcleaner.TagToken) {
            org.htmlcleaner.TagToken tagToken = (org.htmlcleaner.TagToken) (token) ;
            java.lang.String tagName = tagToken.getName();
            if ("script".equals(tagName)) {
                return true;
            }
        }
        if (CONTENT_ALL == contentType) {
            if ( !childTags.isEmpty()) {
                return token instanceof org.htmlcleaner.TagToken ? childTags.contains(((org.htmlcleaner.TagToken) (token) ).getName()) : false;
            }
            else if ( !permittedTags.isEmpty()) {
                return token instanceof org.htmlcleaner.TagToken ?  !permittedTags.contains(((org.htmlcleaner.TagToken) (token) ).getName()) : true;
            }
            return true;
        }
        else if (CONTENT_TEXT == contentType) {
            return  !(token instanceof org.htmlcleaner.TagToken);
        }
        return false;
    }
    boolean allowsAnything() {
        return CONTENT_ALL == contentType && childTags.size() == 0;
    }
}
