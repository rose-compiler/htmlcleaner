/*  Copyright (c) 2006-2007, Vladimir Nikic
	All rights reserved.

	Redistribution and use of this software in source and binary forms,
	with or without modification, are permitted provided that the following
	conditions are met:

	* Redistributions of source code must retain the above
	  copyright notice, this list of conditions and the
	  following disclaimer.

	* Redistributions in binary form must reproduce the above
	  copyright notice, this list of conditions and the
	  following disclaimer in the documentation and/or other
	  materials provided with the distribution.

	* The name of HtmlCleaner may not be used to endorse or promote
	  products derived from this software without specific prior
	  written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.

	You can contact Vladimir Nikic by sending e-mail to
	nikic_vladimir@yahoo.com. Please include the word "HtmlCleaner" in the
	subject line.
*/
/**
 * <p>Command line usage class.</p>
 */
package org.htmlcleaner;
import org.htmlcleaner.*;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;
import java.util.Iterator;
public class CommandLine extends java.lang.Object {
    public CommandLine() {
    }
    private static java.lang.String getArgValue(java.lang.String[] args, java.lang.String name) {
        for (int i = 0; i < args.length; i++) {
            java.lang.String curr = args[i];
            int eqIndex = curr.indexOf('\u003d');
            if (eqIndex >= 0) {
                java.lang.String argName = curr.substring(0,eqIndex).trim();
                java.lang.String argValue = curr.substring(eqIndex + 1).trim();
                if (argName.toLowerCase().startsWith(name.toLowerCase())) {
                    return argValue;
                }
            }
        }
        return "";
    }
    private static boolean toBoolean(java.lang.String s) {
        return s != null && ("on".equalsIgnoreCase(s) || "true".equalsIgnoreCase(s) || "yes".equalsIgnoreCase(s));
    }
    public static void main(java.lang.String[] args) throws org.htmlcleaner.XPatherException, java.io.IOException {
        java.lang.String source = getArgValue(args,"src");
        if ("".equals(source)) {
            java.lang.System.err.println("Usage: java -jar htmlcleanerXX.jar src = <url | file> [incharset = <charset>] [dest = <file>] [outcharset = <charset>] [taginfofile=<file>] [options...]");
            java.lang.System.err.println("");
            java.lang.System.err.println("where options include:");
            java.lang.System.err.println("    outputtype=simple* | compact | browser-compact | pretty | htmlsimple | htmlcompact | htmlpretty");
            java.lang.System.err.println("    advancedxmlescape=true* | false");
            java.lang.System.err.println("    transrescharstoncr=true | false*");
            java.lang.System.err.println("    usecdata=true* | false");
            java.lang.System.err.println("    specialentities=true* | false");
            java.lang.System.err.println("    transspecialentitiestoncr=true | false*");
            java.lang.System.err.println("    unicodechars=true* | false");
            java.lang.System.err.println("    omitunknowntags=true | false*");
            java.lang.System.err.println("    treatunknowntagsascontent=true | false*");
            java.lang.System.err.println("    omitdeprtags=true | false*");
            java.lang.System.err.println("    treatdeprtagsascontent=true | false*");
            java.lang.System.err.println("    omitcomments=true | false*");
            java.lang.System.err.println("    omitxmldecl=true | false*");
            java.lang.System.err.println("    omitdoctypedecl=true* | false");
            java.lang.System.err.println("    useemptyelementtags=true* | false");
            java.lang.System.err.println("    allowmultiwordattributes=true* | false");
            java.lang.System.err.println("    allowhtmlinsideattributes=true | false*");
            java.lang.System.err.println("    ignoreqe=true* | false");
            java.lang.System.err.println("    namespacesaware=true* | false");
            java.lang.System.err.println("    hyphenreplacement=<string value> [=]");
            java.lang.System.err.println("    prunetags=<string value> []");
            java.lang.System.err.println("    booleanatts=self* | empty | true");
            java.lang.System.err.println("    nodebyxpath=<xpath expression>");
            java.lang.System.err.println("    omitenvelope=true | false*");
            java.lang.System.err.println("    t:<sourcetagX>[=<desttag>[,<preserveatts>]]");
            java.lang.System.err.println("    t:<sourcetagX>.<destattrY>[=<template>]");
            java.lang.System.exit(1);
        }
        java.lang.String inCharset = getArgValue(args,"incharset");
        if ("".equals(inCharset)) {
            inCharset = org.htmlcleaner.HtmlCleaner.DEFAULT_CHARSET;
        }
        java.lang.String outCharset = getArgValue(args,"outcharset");
        if ("".equals(outCharset)) {
            outCharset = org.htmlcleaner.HtmlCleaner.DEFAULT_CHARSET;
        }
        java.lang.String destination = getArgValue(args,"dest");
        java.lang.String outputType = getArgValue(args,"outputtype");
        java.lang.String advancedXmlEscape = getArgValue(args,"advancedxmlescape");
        java.lang.String transResCharsToNCR = getArgValue(args,"transrescharstoncr");
        java.lang.String useCData = getArgValue(args,"usecdata");
        java.lang.String translateSpecialEntities = getArgValue(args,"specialentities");
        java.lang.String transSpecialEntitiesToNCR = getArgValue(args,"transspecialentitiestoncr");
        java.lang.String unicodeChars = getArgValue(args,"unicodechars");
        java.lang.String omitUnknownTags = getArgValue(args,"omitunknowntags");
        java.lang.String treatUnknownTagsAsContent = getArgValue(args,"treatunknowntagsascontent");
        java.lang.String omitDeprecatedTags = getArgValue(args,"omitdeprtags");
        java.lang.String treatDeprecatedTagsAsContent = getArgValue(args,"treatdeprtagsascontent");
        java.lang.String omitComments = getArgValue(args,"omitcomments");
        java.lang.String omitXmlDeclaration = getArgValue(args,"omitxmldecl");
        java.lang.String omitDoctypeDeclaration = getArgValue(args,"omitdoctypedecl");
        java.lang.String omitHtmlEnvelope = getArgValue(args,"omithtmlenvelope");
        java.lang.String useEmptyElementTags = getArgValue(args,"useemptyelementtags");
        java.lang.String allowMultiWordAttributes = getArgValue(args,"allowmultiwordattributes");
        java.lang.String allowHtmlInsideAttributes = getArgValue(args,"allowhtmlinsideattributes");
        java.lang.String ignoreQuestAndExclam = getArgValue(args,"ignoreqe");
        java.lang.String namespacesAware = getArgValue(args,"namespacesaware");
        java.lang.String commentHyphen = getArgValue(args,"hyphenreplacement");
        java.lang.String pruneTags = getArgValue(args,"prunetags");
        java.lang.String booleanAtts = getArgValue(args,"booleanatts");
        java.lang.String nodeByXPath = getArgValue(args,"nodebyxpath");
        boolean omitEnvelope = toBoolean(getArgValue(args,"omitenvelope"));
        org.htmlcleaner.HtmlCleaner cleaner;
        java.lang.String tagInfoFile = getArgValue(args,"taginfofile");
        if ( !"".equals(tagInfoFile)) {
            cleaner = new org.htmlcleaner.HtmlCleaner(new org.htmlcleaner.ConfigFileTagProvider(new java.io.File(tagInfoFile)));
        }
        else {
            cleaner = new org.htmlcleaner.HtmlCleaner();
        }
        final org.htmlcleaner.CleanerProperties props = cleaner.getProperties();
        if ( !"".equals(omitUnknownTags)) {
            props.setOmitUnknownTags(toBoolean(omitUnknownTags));
        }
        if ( !"".equals(treatUnknownTagsAsContent)) {
            props.setTreatUnknownTagsAsContent(toBoolean(treatUnknownTagsAsContent));
        }
        if ( !"".equals(omitDeprecatedTags)) {
            props.setOmitDeprecatedTags(toBoolean(omitDeprecatedTags));
        }
        if ( !"".equals(treatDeprecatedTagsAsContent)) {
            props.setTreatDeprecatedTagsAsContent(toBoolean(treatDeprecatedTagsAsContent));
        }
        if ( !"".equals(advancedXmlEscape)) {
            props.setAdvancedXmlEscape(toBoolean(advancedXmlEscape));
        }
        if ( !"".equals(transResCharsToNCR)) {
            props.setTransResCharsToNCR(toBoolean(transResCharsToNCR));
        }
        if ( !"".equals(useCData)) {
            props.setUseCdataForScriptAndStyle(toBoolean(useCData));
        }
        if ( !"".equals(translateSpecialEntities)) {
            props.setTranslateSpecialEntities(toBoolean(translateSpecialEntities));
        }
        if ( !"".equals(transSpecialEntitiesToNCR)) {
            props.setTransSpecialEntitiesToNCR(toBoolean(transSpecialEntitiesToNCR));
        }
        if ( !"".equals(unicodeChars)) {
            props.setRecognizeUnicodeChars(toBoolean(unicodeChars));
        }
        if ( !"".equals(omitComments)) {
            props.setOmitComments(toBoolean(omitComments));
        }
        if ( !"".equals(omitXmlDeclaration)) {
            props.setOmitXmlDeclaration(toBoolean(omitXmlDeclaration));
        }
        if ( !"".equals(omitDoctypeDeclaration)) {
            props.setOmitDoctypeDeclaration(toBoolean(omitDoctypeDeclaration));
        }
        if ( !"".equals(omitHtmlEnvelope)) {
            props.setOmitHtmlEnvelope(toBoolean(omitHtmlEnvelope));
        }
        if ( !"".equals(useEmptyElementTags)) {
            props.setUseEmptyElementTags(toBoolean(useEmptyElementTags));
        }
        if ( !"".equals(allowMultiWordAttributes)) {
            props.setAllowMultiWordAttributes(toBoolean(allowMultiWordAttributes));
        }
        if ( !"".equals(allowHtmlInsideAttributes)) {
            props.setAllowHtmlInsideAttributes(toBoolean(allowHtmlInsideAttributes));
        }
        if ( !"".equals(ignoreQuestAndExclam)) {
            props.setIgnoreQuestAndExclam(toBoolean(ignoreQuestAndExclam));
        }
        if ( !"".equals(namespacesAware)) {
            props.setNamespacesAware(toBoolean(namespacesAware));
        }
        if ( !"".equals(commentHyphen)) {
            props.setHyphenReplacementInComment(commentHyphen);
        }
        if ( !"".equals(pruneTags)) {
            props.setPruneTags(pruneTags);
        }
        if ( !"".equals(booleanAtts)) {
            props.setBooleanAttributeValues(booleanAtts);
        }
// collect transformation info
        java.util.Map transInfos = new java.util.TreeMap();
        for (int i = 0; i < args.length; i++) {
            java.lang.String arg = args[i];
            if (arg.startsWith("t:") && arg.length() > 2) {
                arg = arg.substring(2);
                int index = arg.indexOf('\u003d');
                java.lang.String key = index <= 0 ? arg : arg.substring(0,index);
                java.lang.String value = index <= 0 ? null : arg.substring(index + 1);
                transInfos.put(key,value);
            }
        }
        if (transInfos != null) {
            org.htmlcleaner.CleanerTransformations transformations = new org.htmlcleaner.CleanerTransformations();
            java.util.Iterator iterator = transInfos.entrySet().iterator();
            while (iterator.hasNext()) {
                java.util.Map.Entry entry = (java.util.Map.Entry) (iterator.next()) ;
                java.lang.String tag = (java.lang.String) (entry.getKey()) ;
                java.lang.String value = (java.lang.String) (entry.getValue()) ;
                org.htmlcleaner.Utils.updateTagTransformations(transformations,tag,value);
            }
            cleaner.setTransformations(transformations);
        }
        long start = java.lang.System.currentTimeMillis();
        org.htmlcleaner.TagNode node;
        java.lang.String srcLowerCase = source.toLowerCase();
        if (srcLowerCase.startsWith("http://") || srcLowerCase.startsWith("https://")) {
            node = cleaner.clean(new java.net.URL(source),inCharset);
        }
        else {
            node = cleaner.clean(new java.io.File(source),inCharset);
        }
// if user specifies XPath expresssion to choose node for serialization, then
// try to evaluate XPath and look for first TagNode instance in the resulting array
        if ( !"".equals(nodeByXPath)) {
            final java.lang.Object[] xpathResult = node.evaluateXPath(nodeByXPath);
            int i;
            for (i = 0; i < xpathResult.length; i++) {
                if (xpathResult[i] instanceof org.htmlcleaner.TagNode) {
                    node = (org.htmlcleaner.TagNode) (xpathResult[i]) ;
                    java.lang.System.out.println("Node successfully found by XPath.");
                    break;
                }
            }
            if (i == xpathResult.length) {
                java.lang.System.out.println("Node not found by XPath expression - whole html tree is going to be serialized!");
            }
        }
        java.io.OutputStream out;
        if (destination == null || "".equals(destination.trim())) {
            out = java.lang.System.out;
        }
        else {
            out = new java.io.FileOutputStream(destination);
        }
        if ("compact".equals(outputType)) {
            new org.htmlcleaner.CompactXmlSerializer(props).writeToStream(node,out,outCharset,omitEnvelope);
        }
        else if ("browser-compact".equals(outputType)) {
            new org.htmlcleaner.BrowserCompactXmlSerializer(props).writeToStream(node,out,outCharset,omitEnvelope);
        }
        else if ("pretty".equals(outputType)) {
            new org.htmlcleaner.PrettyXmlSerializer(props).writeToStream(node,out,outCharset,omitEnvelope);
        }
        else if ("htmlsimple".equals(outputType)) {
            new org.htmlcleaner.SimpleHtmlSerializer(props).writeToStream(node,out,outCharset,omitEnvelope);
        }
        else if ("htmlcompact".equals(outputType)) {
            new org.htmlcleaner.CompactHtmlSerializer(props).writeToStream(node,out,outCharset,omitEnvelope);
        }
        else if ("htmlpretty".equals(outputType)) {
            new org.htmlcleaner.PrettyHtmlSerializer(props).writeToStream(node,out,outCharset,omitEnvelope);
        }
        else {
            new org.htmlcleaner.SimpleXmlSerializer(props).writeToStream(node,out,outCharset,omitEnvelope);
        }
        java.lang.System.out.println("Finished successfully in " + (java.lang.System.currentTimeMillis() - start) + "ms.");
    }
}
