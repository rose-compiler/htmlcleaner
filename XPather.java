/*  Copyright (c) 2006-2007, Vladimir Nikic
    All rights reserved.

    Redistribution and use of this software in source and binary forms,
    with or without modification, are permitted provided that the following
    conditions are met:

    * Redistributions of source code must retain the above
      copyright notice, this list of conditions and the
      following disclaimer.

    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the
      following disclaimer in the documentation and/or other
      materials provided with the distribution.

    * The name of HtmlCleaner may not be used to endorse or promote
      products derived from this software without specific prior
      written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    You can contact Vladimir Nikic by sending e-mail to
    nikic_vladimir@yahoo.com. Please include the word "HtmlCleaner" in the
    subject line.
*/
/**
 * <p>Utility for searching cleaned document tree with XPath expressions.</p>
 * Examples of supported axes:
 * <code>
 * <ul>
 *      <li>//div//a</li>  
 *      <li>//div//a[@id][@class]</li>  
 *      <li>/body/*[1]/@type</li>
 *      <li>//div[3]//a[@id][@href='r/n4']</li>  
 *      <li>//div[last() >= 4]//./div[position() = last()])[position() > 22]//li[2]//a</li>  
 *      <li>//div[2]/@*[2]</li>  
 *      <li>data(//div//a[@id][@class])</li>  
 *      <li>//p/last()</li>  
 *      <li>//body//div[3][@class]//span[12.2<position()]/@id</li>  
 *      <li>data(//a['v' < @id])</li>  
 * </ul>
 * </code>
 */
package org.htmlcleaner;
import org.htmlcleaner.*;
import java.util.*;
public class XPather extends java.lang.Object {
// array of basic tokens of which XPath expression is made
    private java.lang.String[] tokenArray;
/**
     * Constructor - creates XPather instance with specified XPath expression.
     * @param expression
     */
    public XPather(java.lang.String expression) {
        super();
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(expression, "/()[]\"'=<>", true);
        int tokenCount = tokenizer.countTokens();
        tokenArray = new java.lang.String[tokenCount];
        int index = 0;
// this is not real XPath compiler, rather simple way to recognize basic XPaths expressions
// and interpret them against some TagNode instance.
        while (tokenizer.hasMoreTokens()) {
            tokenArray[index++] = tokenizer.nextToken();
        }
    }
/**
     * Main public method for this class - a way to execute XPath expression against
     * specified TagNode instance.
     * @param node
     */
    public java.lang.Object[] evaluateAgainstNode(org.htmlcleaner.TagNode node) throws org.htmlcleaner.XPatherException {
        if (node == null) {
            throw new org.htmlcleaner.XPatherException("Cannot evaluate XPath expression against null value!");
        }
        java.util.Collection collectionResult = this.evaluateAgainst(this.singleton(node),0,tokenArray.length - 1,false,1,0,false,null);
        java.lang.Object[] array = new java.lang.Object[collectionResult.size()];
        java.util.Iterator iterator = collectionResult.iterator();
        int index = 0;
        while (iterator.hasNext()) {
            array[index++] = iterator.next();
        }
        return array;
    }
    private void throwStandardException() throws org.htmlcleaner.XPatherException {
        throw new org.htmlcleaner.XPatherException();
    }
    private java.util.Collection evaluateAgainst(java.util.Collection object, int from, int to, boolean isRecursive, int position, int last, boolean isFilterContext, java.util.Collection filterSource) throws org.htmlcleaner.XPatherException {
        if (from >= 0 && to < tokenArray.length && from <= to) {
            if ("".equals(tokenArray[from].trim())) {
                return this.evaluateAgainst(object,from + 1,to,isRecursive,position,last,isFilterContext,filterSource);
            }
            else if (this.isToken("(",from)) {
                int closingBracket = this.findClosingIndex(from,to);
                if (closingBracket > 0) {
                    java.util.Collection value = this.evaluateAgainst(object,from + 1,closingBracket - 1,false,position,last,isFilterContext,filterSource);
                    return this.evaluateAgainst(value,closingBracket + 1,to,false,position,last,isFilterContext,filterSource);
                }
                else {
                    this.throwStandardException();
                }
            }
            else if (this.isToken("[",from)) {
                int closingBracket = this.findClosingIndex(from,to);
                if (closingBracket > 0 && object instanceof java.util.Collection) {
                    java.util.Collection value = this.filterByCondition(object,from + 1,closingBracket - 1);
                    return this.evaluateAgainst(value,closingBracket + 1,to,false,position,last,isFilterContext,filterSource);
                }
                else {
                    this.throwStandardException();
                }
            }
            else if (this.isToken("\"",from) || this.isToken("'",from)) {
                int closingQuote = this.findClosingIndex(from,to);
                if (closingQuote > from) {
                    java.util.Collection value = this.singleton(this.flatten(from + 1,closingQuote - 1));
                    return this.evaluateAgainst(value,closingQuote + 1,to,false,position,last,isFilterContext,filterSource);
                }
                else {
                    this.throwStandardException();
                }
            }
            else if ((this.isToken("=",from) || this.isToken("<",from) || this.isToken(">",from)) && isFilterContext) {
                boolean logicValue;
                if (this.isToken("=",from + 1) && (this.isToken("<",from) || this.isToken(">",from))) {
                    java.util.Collection secondObject = this.evaluateAgainst(filterSource,from + 2,to,false,position,last,isFilterContext,filterSource);
                    logicValue = this.evaluateLogic(object,secondObject,tokenArray[from] + tokenArray[from + 1]);
                }
                else {
                    java.util.Collection secondObject = this.evaluateAgainst(filterSource,from + 1,to,false,position,last,isFilterContext,filterSource);
                    logicValue = this.evaluateLogic(object,secondObject,tokenArray[from]);
                }
                return this.singleton(new java.lang.Boolean(logicValue));
            }
            else if (this.isToken("/",from)) {
                boolean goRecursive = this.isToken("/",from + 1);
                if (goRecursive) {
                    from++;
                }
                if (from < to) {
                    int toIndex = this.findClosingIndex(from,to) - 1;
                    if (toIndex <= from) {
                        toIndex = to;
                    }
                    java.util.Collection value = this.evaluateAgainst(object,from + 1,toIndex,goRecursive,1,last,isFilterContext,filterSource);
                    return this.evaluateAgainst(value,toIndex + 1,to,false,1,last,isFilterContext,filterSource);
                }
                else {
                    this.throwStandardException();
                }
            }
            else if (this.isFunctionCall(from,to)) {
                int closingBracketIndex = this.findClosingIndex(from + 1,to);
                java.util.Collection funcValue = this.evaluateFunction(object,from,to,position,last,isFilterContext);
                return this.evaluateAgainst(funcValue,closingBracketIndex + 1,to,false,1,last,isFilterContext,filterSource);
            }
            else if (this.isValidInteger(tokenArray[from])) {
                java.util.Collection value = this.singleton(new java.lang.Integer(tokenArray[from]));
                return this.evaluateAgainst(value,from + 1,to,false,position,last,isFilterContext,filterSource);
            }
            else if (this.isValidDouble(tokenArray[from])) {
                java.util.Collection value = this.singleton(new java.lang.Double(tokenArray[from]));
                return this.evaluateAgainst(value,from + 1,to,false,position,last,isFilterContext,filterSource);
            }
            else {
                return this.getElementsByName(object,from,to,isRecursive,isFilterContext);
            }
        }
        else {
            return object;
        }
        throw new org.htmlcleaner.XPatherException();
    }
    private java.lang.String flatten(int from, int to) {
        if (from <= to) {
            java.lang.StringBuffer result = new java.lang.StringBuffer();
            for (int i = from; i <= to; i++) {
                result.append(tokenArray[i]);
            }
            return result.toString();
        }
        return "";
    }
    private boolean isValidInteger(java.lang.String s) {
        try {
            java.lang.Integer.parseInt(s);
            return true;
        }
        catch (java.lang.NumberFormatException e){
            return false;
        }
    }
    private boolean isValidDouble(java.lang.String s) {
        try {
            java.lang.Double.parseDouble(s);
            return true;
        }
        catch (java.lang.NumberFormatException e){
            return false;
        }
    }
/**
     * Checks if given string is valid identifier.
     * @param s
     */
    private boolean isIdentifier(java.lang.String s) {
        if (s == null) {
            return false;
        }
        s = s.trim();
        if (s.length() > 0) {
            if ( !java.lang.Character.isLetter(s.charAt(0))) {
                return false;
            }
            for (int i = 1; i < s.length(); i++) {
                final char ch = s.charAt(i);
                if (ch != '\u005f' && ch != '\u002d' &&  !java.lang.Character.isLetterOrDigit(ch)) {
                    return false;
                }
            }
        }
        return false;
    }
/**
     * Checks if tokens in specified range represents valid function call.
     * @param from
     * @param to
     * @return True if it is valid function call, false otherwise. 
     */
    private boolean isFunctionCall(int from, int to) {
        if ( !this.isIdentifier(tokenArray[from]) &&  !this.isToken("(",from + 1)) {
            return false;
        }
        return this.findClosingIndex(from + 1,to) > from + 1;
    }
/**
     * Evaluates specified function.
     * Currently, following XPath functions are supported: last, position, text, count, data 
     * @param source
     * @param from
     * @param to
     * @param position
     * @param last
     * @return Collection as the result of evaluation.
     */
    private java.util.Collection evaluateFunction(java.util.Collection source, int from, int to, int position, int last, boolean isFilterContext) throws org.htmlcleaner.XPatherException {
        java.lang.String name = tokenArray[from].trim();
        java.util.ArrayList result = new java.util.ArrayList();
        final int size = source.size();
        java.util.Iterator iterator = source.iterator();
        int index = 0;
        while (iterator.hasNext()) {
            java.lang.Object curr = iterator.next();
            index++;
            if ("last".equals(name)) {
                result.add(new java.lang.Integer(isFilterContext ? last : size));
            }
            else if ("position".equals(name)) {
                result.add(new java.lang.Integer(isFilterContext ? position : index));
            }
            else if ("text".equals(name)) {
                if (curr instanceof org.htmlcleaner.TagNode) {
                    result.add(((org.htmlcleaner.TagNode) (curr) ).getText());
                }
                else if (curr instanceof java.lang.String) {
                    result.add(curr.toString());
                }
            }
            else if ("count".equals(name)) {
                java.util.Collection argumentEvaluated = this.evaluateAgainst(source,from + 2,to - 1,false,position,0,isFilterContext,null);
                result.add(new java.lang.Integer(argumentEvaluated.size()));
            }
            else if ("data".equals(name)) {
                java.util.Collection argumentEvaluated = this.evaluateAgainst(source,from + 2,to - 1,false,position,0,isFilterContext,null);
                java.util.Iterator it = argumentEvaluated.iterator();
                while (it.hasNext()) {
                    java.lang.Object elem = it.next();
                    if (elem instanceof org.htmlcleaner.TagNode) {
                        result.add(((org.htmlcleaner.TagNode) (elem) ).getText());
                    }
                    else if (elem instanceof java.lang.String) {
                        result.add(elem.toString());
                    }
                }
            }
            else {
                throw new org.htmlcleaner.XPatherException("Unknown function " + name + "!");
            }
        }
        return result;
    }
/**
     * Filter nodes satisfying the condition
     * @param source
     * @param from
     * @param to
     */
    private java.util.Collection filterByCondition(java.util.Collection source, int from, int to) throws org.htmlcleaner.XPatherException {
        java.util.ArrayList result = new java.util.ArrayList();
        java.util.Iterator iterator = source.iterator();
        int index = 0;
        int size = source.size();
        while (iterator.hasNext()) {
            java.lang.Object curr = iterator.next();
            index++;
            java.util.ArrayList logicValueList = new java.util.ArrayList(this.evaluateAgainst(this.singleton(curr),from,to,false,index,size,true,this.singleton(curr)));
            if (logicValueList.size() >= 1) {
                java.lang.Object first = logicValueList.get(0);
                if (first instanceof java.lang.Boolean) {
                    if (((java.lang.Boolean) (first) ).booleanValue()) {
                        result.add(curr);
                    }
                }
                else if (first instanceof java.lang.Integer) {
                    if (((java.lang.Integer) (first) ).intValue() == index) {
                        result.add(curr);
                    }
                }
                else {
                    result.add(curr);
                }
            }
        }
        return result;
    }
    private boolean isToken(java.lang.String token, int index) {
        int len = tokenArray.length;
        return index >= 0 && index < len && tokenArray[index].trim().equals(token.trim());
    }
/**
     * @param from
     * @param to
     * @return matching closing index in the token array for the current token, or -1 if there is
     * no closing token within expected bounds.
     */
    private int findClosingIndex(int from, int to) {
        if (from < to) {
            java.lang.String currToken = tokenArray[from];
            if ("\"".equals(currToken)) {
                for (int i = from + 1; i <= to; i++) {
                    if ("\"".equals(tokenArray[i])) {
                        return i;
                    }
                }
            }
            else if ("'".equals(currToken)) {
                for (int i = from + 1; i <= to; i++) {
                    if ("'".equals(tokenArray[i])) {
                        return i;
                    }
                }
            }
            else if ("(".equals(currToken) || "[".equals(currToken) || "/".equals(currToken)) {
                boolean isQuoteClosed = true;
                boolean isAposClosed = true;
                int brackets = "(".equals(currToken) ? 1 : 0;
                int angleBrackets = "[".equals(currToken) ? 1 : 0;
                int slashes = "/".equals(currToken) ? 1 : 0;
                for (int i = from + 1; i <= to; i++) {
                    if ("\"".equals(tokenArray[i])) {
                        isQuoteClosed =  !isQuoteClosed;
                    }
                    else if ("'".equals(tokenArray[i])) {
                        isAposClosed =  !isAposClosed;
                    }
                    else if ("(".equals(tokenArray[i]) && isQuoteClosed && isAposClosed) {
                        brackets++;
                    }
                    else if (")".equals(tokenArray[i]) && isQuoteClosed && isAposClosed) {
                        brackets--;
                    }
                    else if ("[".equals(tokenArray[i]) && isQuoteClosed && isAposClosed) {
                        angleBrackets++;
                    }
                    else if ("]".equals(tokenArray[i]) && isQuoteClosed && isAposClosed) {
                        angleBrackets--;
                    }
                    else if ("/".equals(tokenArray[i]) && isQuoteClosed && isAposClosed && brackets == 0 && angleBrackets == 0) {
                        slashes--;
                    }
                    if (isQuoteClosed && isAposClosed && brackets == 0 && angleBrackets == 0 && slashes == 0) {
                        return i;
                    }
                }
            }
        }
        return  -1;
    }
/**
     * Checks if token is attribute (starts with @)
     * @param token
     */
    private boolean isAtt(java.lang.String token) {
        return token != null && token.length() > 1 && token.startsWith("@");
    }
/**
     * Creates one-element collection for the specified object.
     * @param element
     */
    private java.util.Collection singleton(java.lang.Object element) {
        java.util.ArrayList result = new java.util.ArrayList();
        result.add(element);
        return result;
    }
/**
     * For the given source collection and specified name, returns collection of subnodes
     * or attribute values.
     * @param source
     * @param from
     * @param to
     * @param isRecursive
     * @return Colection of TagNode instances or collection of String instances.              
     */
    private java.util.Collection getElementsByName(java.util.Collection source, int from, int to, boolean isRecursive, boolean isFilterContext) throws org.htmlcleaner.XPatherException {
        java.lang.String name = tokenArray[from].trim();
        if (this.isAtt(name)) {
            name = name.substring(1);
            java.util.Collection result = new java.util.ArrayList();
            java.util.Collection nodes;
            if (isRecursive) {
                nodes = new java.util.LinkedHashSet();
                java.util.Iterator iterator = source.iterator();
                while (iterator.hasNext()) {
                    java.lang.Object next = iterator.next();
                    if (next instanceof org.htmlcleaner.TagNode) {
                        org.htmlcleaner.TagNode node = (org.htmlcleaner.TagNode) (next) ;
                        nodes.addAll(node.getAllElementsList(true));
                    }
                }
            }
            else {
                nodes = source;
            }
            java.util.Iterator iterator = nodes.iterator();
            while (iterator.hasNext()) {
                java.lang.Object next = iterator.next();
                if (next instanceof org.htmlcleaner.TagNode) {
                    org.htmlcleaner.TagNode node = (org.htmlcleaner.TagNode) (next) ;
                    if ("*".equals(name)) {
                        result.addAll(this.evaluateAgainst(node.getAttributes().values(),from + 1,to,false,1,1,isFilterContext,null));
                    }
                    else {
                        java.lang.String attValue = node.getAttributeByName(name);
                        if (attValue != null) {
                            result.addAll(this.evaluateAgainst(this.singleton(attValue),from + 1,to,false,1,1,isFilterContext,null));
                        }
                    }
                }
                else {
                    this.throwStandardException();
                }
            }
            return result;
        }
        else {
            java.util.Collection result = new java.util.LinkedHashSet();
            java.util.Iterator iterator = source.iterator();
            int index = 0;
            while (iterator.hasNext()) {
                final java.lang.Object next = iterator.next();
                if (next instanceof org.htmlcleaner.TagNode) {
                    org.htmlcleaner.TagNode node = (org.htmlcleaner.TagNode) (next) ;
                    index++;
                    boolean isSelf = ".".equals(name);
                    boolean isParent = "..".equals(name);
                    boolean isAll = "*".equals(name);
                    java.util.Collection subnodes;
                    if (isSelf) {
                        subnodes = this.singleton(node);
                    }
                    else if (isParent) {
                        org.htmlcleaner.TagNode parent = node.getParent();
                        subnodes = parent != null ? this.singleton(parent) : new java.util.ArrayList();
                    }
                    else {
                        subnodes = isAll ? node.getChildTagList() : node.getElementListByName(name,false);
                    }
                    java.util.LinkedHashSet nodeSet = new java.util.LinkedHashSet(subnodes);
                    java.util.Collection refinedSubnodes = this.evaluateAgainst(nodeSet,from + 1,to,false,index,nodeSet.size(),isFilterContext,null);
                    if (isRecursive) {
                        java.util.List childTags = node.getChildTagList();
                        if (isSelf || isParent || isAll) {
                            result.addAll(refinedSubnodes);
                        }
                        java.util.Iterator childIterator = childTags.iterator();
                        while (childIterator.hasNext()) {
                            org.htmlcleaner.TagNode childTag = (org.htmlcleaner.TagNode) (childIterator.next()) ;
                            java.util.Collection childrenByName = this.getElementsByName(this.singleton(childTag),from,to,isRecursive,isFilterContext);
                            if ( !isSelf &&  !isParent &&  !isAll && refinedSubnodes.contains(childTag)) {
                                result.add(childTag);
                            }
                            result.addAll(childrenByName);
                        }
                    }
                    else {
                        result.addAll(refinedSubnodes);
                    }
                }
                else {
                    this.throwStandardException();
                }
            }
            return result;
        }
    }
/**
     * Evaluates logic operation on two collections.
     * @param first
     * @param second
     * @param logicOperator
     * @return Result of logic operation
     */
    private boolean evaluateLogic(java.util.Collection first, java.util.Collection second, java.lang.String logicOperator) {
        if (first == null || first.size() == 0 || second == null || second.size() == 0) {
            return false;
        }
        java.lang.Object elem1 = first.iterator().next();
        java.lang.Object elem2 = second.iterator().next();
        if (elem1 instanceof java.lang.Number && elem2 instanceof java.lang.Number) {
            double d1 = ((java.lang.Number) (elem1) ).doubleValue();
            double d2 = ((java.lang.Number) (elem2) ).doubleValue();
            if ("=".equals(logicOperator)) {
                return d1 == d2;
            }
            else if ("<".equals(logicOperator)) {
                return d1 < d2;
            }
            else if (">".equals(logicOperator)) {
                return d1 > d2;
            }
            else if ("<=".equals(logicOperator)) {
                return d1 <= d2;
            }
            else if (">=".equals(logicOperator)) {
                return d1 >= d2;
            }
        }
        else {
            java.lang.String s1 = this.toText(elem1);
            java.lang.String s2 = this.toText(elem2);
            int result = s1.compareTo(s2);
            if ("=".equals(logicOperator)) {
                return result == 0;
            }
            else if ("<".equals(logicOperator)) {
                return result < 0;
            }
            else if (">".equals(logicOperator)) {
                return result > 0;
            }
            else if ("<=".equals(logicOperator)) {
                return result <= 0;
            }
            else if (">=".equals(logicOperator)) {
                return result >= 0;
            }
        }
        return false;
    }
    private java.lang.String toText(java.lang.Object o) {
        if (o == null) {
            return "";
        }
        if (o instanceof org.htmlcleaner.TagNode) {
            return ((org.htmlcleaner.TagNode) (o) ).getText().toString();
        }
        else {
            return o.toString();
        }
    }
}
