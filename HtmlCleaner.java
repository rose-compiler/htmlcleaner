/*  Copyright (c) 2006-2007, Vladimir Nikic
    All rights reserved.

    Redistribution and use of this software in source and binary forms,
    with or without modification, are permitted provided that the following
    conditions are met:

    * Redistributions of source code must retain the above
      copyright notice, this list of conditions and the
      following disclaimer.

    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the
      following disclaimer in the documentation and/or other
      materials provided with the distribution.

    * The name of HtmlCleaner may not be used to endorse or promote
      products derived from this software without specific prior
      written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    You can contact Vladimir Nikic by sending e-mail to
    nikic_vladimir@yahoo.com. Please include the word "HtmlCleaner" in the
    subject line.
*/
/**
 * Main HtmlCleaner class.
 *
 * <p>It represents public interface to the user. It's task is to call tokenizer with
 * specified source HTML, traverse list of produced token list and create internal
 * object model. It also offers a set of methods to write resulting XML to string,
 * file or any output stream.</p>
 * <p>Typical usage is the following:</p>
 *
 * <xmp>
 *    // create an instance of HtmlCleaner
 *   HtmlCleaner cleaner = new HtmlCleaner();
 *
 *   // take default cleaner properties
 *   CleanerProperties props = cleaner.getProperties();
 *
 *   // customize cleaner's behaviour with property setters
 *   props.setXXX(...);
 *
 *   // Clean HTML taken from simple string, file, URL, input stream,
 *   // input source or reader. Result is root node of created
 *   // tree-like structure. Single cleaner instance may be safely used
 *   // multiple times.
 *   TagNode node = cleaner.clean(...);
 *
 *   // optionally find parts of the DOM or modify some nodes
 *   TagNode[] myNodes = node.getElementsByXXX(...);
 *   // and/or
 *   Object[] myNodes = node.evaluateXPath(xPathExpression);
 *   // and/or
 *   aNode.removeFromTree();
 *   // and/or
 *   aNode.addAttribute(attName, attValue);
 *   // and/or
 *   aNode.removeAttribute(attName, attValue);
 *   // and/or
 *   cleaner.setInnerHtml(aNode, htmlContent);
 *   // and/or do some other tree manipulation/traversal
 *
 *   // serialize a node to a file, output stream, DOM, JDom...
 *   new XXXSerializer(props).writeXmlXXX(aNode, ...);
 *   myJDom = new JDomSerializer(props, true).createJDom(aNode);
 *   myDom = new DomSerializer(props, true).createDOM(aNode);
 * </xmp>
 */
package org.htmlcleaner;
import org.htmlcleaner.*;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
public class HtmlCleaner extends java.lang.Object {
/**
     * Contains information about single open tag
     */
/**
     * Class that contains information and mathods for managing list of open,
     * but unhandled tags.
     */
// do not search past a fatal tag for this tag
/**
         * Checks if any of tags specified in the set are already open.
         * @param tags
         */
    private class CleanTimeValues extends java.lang.Object {
        public CleanTimeValues() {
        }
        private org.htmlcleaner.HtmlCleaner.OpenTags _openTags;
        private boolean _headOpened = false;
        private boolean _bodyOpened = false;
        private java.util.Set _headTags = new java.util.LinkedHashSet();
        private java.util.Set allTags = new java.util.TreeSet();
        private org.htmlcleaner.TagNode htmlNode;
        private org.htmlcleaner.TagNode bodyNode;
        private org.htmlcleaner.TagNode headNode;
        private org.htmlcleaner.TagNode rootNode;
        private java.util.Set<java.lang.String> pruneTagSet = new java.util.HashSet<java.lang.String>();
        private java.util.Set<org.htmlcleaner.TagNode> pruneNodeSet = new java.util.HashSet<org.htmlcleaner.TagNode>();
    }
    private class OpenTags extends java.lang.Object {
        public OpenTags() {
        }
        private java.util.List<org.htmlcleaner.HtmlCleaner.TagPos> list = new java.util.ArrayList<org.htmlcleaner.HtmlCleaner.TagPos>();
        private org.htmlcleaner.HtmlCleaner.TagPos last = null;
        private java.util.Set<java.lang.String> set = new java.util.HashSet<java.lang.String>();
        private boolean isEmpty() {
            return list.isEmpty();
        }
        private void addTag(java.lang.String tagName, int position) {
            last = new org.htmlcleaner.HtmlCleaner.TagPos(position, tagName);
            list.add(last);
            set.add(tagName);
        }
        private void removeTag(java.lang.String tagName) {
            java.util.ListIterator<org.htmlcleaner.HtmlCleaner.TagPos> it = list.listIterator(list.size());
            while (it.hasPrevious()) {
                org.htmlcleaner.HtmlCleaner.TagPos currTagPos = it.previous();
                if (tagName.equals(currTagPos.name)) {
                    it.remove();
                    break;
                }
            }
            last = list.isEmpty() ? null : list.get(list.size() - 1);
        }
        private org.htmlcleaner.HtmlCleaner.TagPos findFirstTagPos() {
            return list.isEmpty() ? null : list.get(0);
        }
        private org.htmlcleaner.HtmlCleaner.TagPos getLastTagPos() {
            return last;
        }
        private org.htmlcleaner.HtmlCleaner.TagPos findTag(java.lang.String tagName) {
            if (tagName != null) {
                java.util.ListIterator<org.htmlcleaner.HtmlCleaner.TagPos> it = list.listIterator(list.size());
                java.lang.String fatalTag = null;
                org.htmlcleaner.TagInfo fatalInfo = tagInfoProvider.getTagInfo(tagName);
                if (fatalInfo != null) {
                    fatalTag = fatalInfo.getFatalTag();
                }
                while (it.hasPrevious()) {
                    org.htmlcleaner.HtmlCleaner.TagPos currTagPos = it.previous();
                    if (tagName.equals(currTagPos.name)) {
                        return currTagPos;
                    }
                    else if (fatalTag != null && fatalTag.equals(currTagPos.name)) {
                        return null;
                    }
                }
            }
            return null;
        }
        private boolean tagExists(java.lang.String tagName) {
            org.htmlcleaner.HtmlCleaner.TagPos tagPos = this.findTag(tagName);
            return tagPos != null;
        }
        private org.htmlcleaner.HtmlCleaner.TagPos findTagToPlaceRubbish() {
            org.htmlcleaner.HtmlCleaner.TagPos result = null;
            org.htmlcleaner.HtmlCleaner.TagPos prev = null;
            if ( !this.isEmpty()) {
                java.util.ListIterator<org.htmlcleaner.HtmlCleaner.TagPos> it = list.listIterator(list.size());
                while (it.hasPrevious()) {
                    result = it.previous();
                    if (result.info == null || result.info.allowsAnything()) {
                        if (prev != null) {
                            return prev;
                        }
                    }
                    prev = result;
                }
            }
            return result;
        }
        private boolean tagEncountered(java.lang.String tagName) {
            return set.contains(tagName);
        }
        private boolean someAlreadyOpen(java.util.Set tags) {
            java.util.Iterator<org.htmlcleaner.HtmlCleaner.TagPos> it = list.iterator();
            while (it.hasNext()) {
                org.htmlcleaner.HtmlCleaner.TagPos curr = it.next();
                if (tags.contains(curr.name)) {
                    return true;
                }
            }
            return false;
        }
    }
    private class TagPos extends java.lang.Object {
        private int position;
        private java.lang.String name;
        private org.htmlcleaner.TagInfo info;
        public TagPos(int position, java.lang.String name) {
            super();
            this.position = position;
            this.name = name;
            this.info = tagInfoProvider.getTagInfo(name);
        }
    }
    final public static java.lang.String DEFAULT_CHARSET = java.lang.System.getProperty("file.encoding");
    private org.htmlcleaner.CleanerProperties properties;
    private org.htmlcleaner.ITagInfoProvider tagInfoProvider;
    private org.htmlcleaner.CleanerTransformations transformations = null;
/**
     * Constructor - creates cleaner instance with default tag info provider and default properties.
     */
    public HtmlCleaner() {
        this(null,null);
    }
/**
     * Constructor - creates the instance with specified tag info provider and default properties
     * @param tagInfoProvider Provider for tag filtering and balancing
     */
    public HtmlCleaner(org.htmlcleaner.ITagInfoProvider tagInfoProvider) {
        this(tagInfoProvider,null);
    }
/**
     * Constructor - creates the instance with default tag info provider and specified properties
     * @param properties Properties used during parsing and serializing
     */
    public HtmlCleaner(org.htmlcleaner.CleanerProperties properties) {
        this(null,properties);
    }
/**
	 * Constructor - creates the instance with specified tag info provider and specified properties
	 * @param tagInfoProvider Provider for tag filtering and balancing
	 * @param properties Properties used during parsing and serializing
	 */
    public HtmlCleaner(org.htmlcleaner.ITagInfoProvider tagInfoProvider, org.htmlcleaner.CleanerProperties properties) {
        super();
        this.tagInfoProvider = tagInfoProvider == null ? org.htmlcleaner.DefaultTagProvider.getInstance() : tagInfoProvider;
        this.properties = properties == null ? new org.htmlcleaner.CleanerProperties() : properties;
        this.properties.tagInfoProvider = this.tagInfoProvider;
    }
    public org.htmlcleaner.TagNode clean(java.lang.String htmlContent) {
        try {
            return this.clean(new java.io.StringReader(htmlContent));
        }
        catch (java.io.IOException e){
// should never happen because reading from StringReader
            throw new org.htmlcleaner.HtmlCleanerException(e);
        }
    }
    public org.htmlcleaner.TagNode clean(java.io.File file, java.lang.String charset) throws java.io.IOException {
        java.io.FileInputStream in = new java.io.FileInputStream(file);
        java.io.Reader reader = new java.io.InputStreamReader(in, charset);
        return this.clean(reader);
    }
    public org.htmlcleaner.TagNode clean(java.io.File file) throws java.io.IOException {
        return this.clean(file,DEFAULT_CHARSET);
    }
    public org.htmlcleaner.TagNode clean(java.net.URL url, java.lang.String charset) throws java.io.IOException {
        java.net.URLConnection urlConnection = url.openConnection();
        if (charset == null) {
            charset = org.htmlcleaner.Utils.getCharsetFromContentTypeString(urlConnection.getHeaderField("Content-Type"));
        }
        if (charset == null) {
            charset = org.htmlcleaner.Utils.getCharsetFromContent(url);
        }
        if (charset == null) {
            charset = DEFAULT_CHARSET;
        }
        return this.clean(url.openStream(),charset);
    }
/**
     * Creates instance from the content downloaded from specified URL.
     * HTML encoding is resolved following the attempts in the sequence:
     * 1. reading Content-Type response header, 2. Analyzing META tags at the
     * beginning of the html, 3. Using platform's default charset.
     * @param url
     * @return
     * @throws IOException
     */
    public org.htmlcleaner.TagNode clean(java.net.URL url) throws java.io.IOException {
        return this.clean(url,null);
    }
    public org.htmlcleaner.TagNode clean(java.io.InputStream in, java.lang.String charset) throws java.io.IOException {
        return this.clean(new java.io.InputStreamReader(in, charset));
    }
    public org.htmlcleaner.TagNode clean(java.io.InputStream in) throws java.io.IOException {
        return this.clean(in,DEFAULT_CHARSET);
    }
    public org.htmlcleaner.TagNode clean(java.io.Reader reader) throws java.io.IOException {
        return this.clean(reader,new org.htmlcleaner.HtmlCleaner.CleanTimeValues());
    }
/**
     * Basic version of the cleaning call.
     * @param reader
     * @return An instance of TagNode object which is the root of the XML tree.
     * @throws IOException
     */
    public org.htmlcleaner.TagNode clean(java.io.Reader reader, final org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) throws java.io.IOException {
        cleanTimeValues._openTags = new org.htmlcleaner.HtmlCleaner.OpenTags();
        cleanTimeValues._headOpened = false;
        cleanTimeValues._bodyOpened = false;
        cleanTimeValues._headTags.clear();
        cleanTimeValues.allTags.clear();
        this.setPruneTags(properties.pruneTags,cleanTimeValues);
        cleanTimeValues.htmlNode = this.createTagNode("html",cleanTimeValues);
        cleanTimeValues.bodyNode = this.createTagNode("body",cleanTimeValues);
        cleanTimeValues.headNode = this.createTagNode("head",cleanTimeValues);
        cleanTimeValues.rootNode = null;
        cleanTimeValues.htmlNode.addChild(cleanTimeValues.headNode);
        cleanTimeValues.htmlNode.addChild(cleanTimeValues.bodyNode);
        org.htmlcleaner.HtmlTokenizer htmlTokenizer = new org.htmlcleaner.HtmlTokenizer(reader, properties, transformations, tagInfoProvider) {
            void makeTree(java.util.List<org.htmlcleaner.BaseToken> tokenList) {
                org.htmlcleaner.HtmlCleaner.this.makeTree(tokenList,tokenList.listIterator(tokenList.size() - 1),cleanTimeValues);
            }
            org.htmlcleaner.TagNode createTagNode(java.lang.String name) {
                return org.htmlcleaner.HtmlCleaner.this.createTagNode(name,cleanTimeValues);
            }
        };
        htmlTokenizer.start();
        java.util.List<org.htmlcleaner.BaseToken> nodeList = htmlTokenizer.getTokenList();
        this.closeAll(nodeList,cleanTimeValues);
        this.createDocumentNodes(nodeList,cleanTimeValues);
        this.calculateRootNode(cleanTimeValues);
// if there are some nodes to prune from tree
        if (cleanTimeValues.pruneNodeSet != null &&  !cleanTimeValues.pruneNodeSet.isEmpty()) {
            java.util.Iterator iterator = cleanTimeValues.pruneNodeSet.iterator();
            while (iterator.hasNext()) {
                org.htmlcleaner.TagNode tagNode = (org.htmlcleaner.TagNode) (iterator.next()) ;
                org.htmlcleaner.TagNode parent = tagNode.getParent();
                if (parent != null) {
                    parent.removeChild(tagNode);
                }
            }
        }
        cleanTimeValues.rootNode.setDocType(htmlTokenizer.getDocType());
        return cleanTimeValues.rootNode;
    }
    private org.htmlcleaner.TagNode createTagNode(java.lang.String name, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        org.htmlcleaner.TagNode node = new org.htmlcleaner.TagNode(name);
        if (cleanTimeValues.pruneTagSet != null && name != null && cleanTimeValues.pruneTagSet.contains(name.toLowerCase())) {
            cleanTimeValues.pruneNodeSet.add(node);
        }
        return node;
    }
    private org.htmlcleaner.TagNode makeTagNodeCopy(org.htmlcleaner.TagNode tagNode, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        org.htmlcleaner.TagNode copy = tagNode.makeCopy();
        if (cleanTimeValues.pruneTagSet != null && cleanTimeValues.pruneTagSet.contains(tagNode.getName())) {
            cleanTimeValues.pruneNodeSet.add(copy);
        }
        return copy;
    }
/**
     * Assigns root node to internal variable.
     * Root node of the result depends on parameter "omitHtmlEnvelope".
     * If it is set, then first child of the body will be root node,
     * or html will be root node otherwise.
     */
    private void calculateRootNode(org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        cleanTimeValues.rootNode = cleanTimeValues.htmlNode;
        if (properties.omitHtmlEnvelope) {
            java.util.List bodyChildren = cleanTimeValues.bodyNode.getChildren();
            if (bodyChildren != null) {
                for (java.lang.Object child : bodyChildren){
// if found child that is tag itself, then return it
                    if (child instanceof org.htmlcleaner.TagNode) {
                        cleanTimeValues.rootNode = (org.htmlcleaner.TagNode) (child) ;
                        break;
                    }
                }
;
            }
        }
    }
/**
     * Add attributes from specified map to the specified tag.
     * If some attribute already exist it is preserved.
     * @param tag
     * @param attributes
     */
    private void addAttributesToTag(org.htmlcleaner.TagNode tag, java.util.Map attributes) {
        if (attributes != null) {
            java.util.Map tagAttributes = tag.getAttributes();
            java.util.Iterator it = attributes.entrySet().iterator();
            while (it.hasNext()) {
                java.util.Map.Entry currEntry = (java.util.Map.Entry) (it.next()) ;
                java.lang.String attName = (java.lang.String) (currEntry.getKey()) ;
                if ( !tagAttributes.containsKey(attName)) {
                    java.lang.String attValue = (java.lang.String) (currEntry.getValue()) ;
                    tag.setAttribute(attName,attValue);
                }
            }
        }
    }
/**
     * Checks if open fatal tag is missing if there is a fatal tag for
     * the specified tag.
     * @param tag
     */
    private boolean isFatalTagSatisfied(org.htmlcleaner.TagInfo tag, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        if (tag != null) {
            java.lang.String fatalTagName = tag.getFatalTag();
            return fatalTagName == null ? true : cleanTimeValues._openTags.tagExists(fatalTagName);
        }
        return true;
    }
/**
     * Check if specified tag requires parent tag, but that parent
     * tag is missing in the appropriate context.
     * @param tag
     */
    private boolean mustAddRequiredParent(org.htmlcleaner.TagInfo tag, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        if (tag != null) {
            java.lang.String requiredParent = tag.getRequiredParent();
            if (requiredParent != null) {
                java.lang.String fatalTag = tag.getFatalTag();
                int fatalTagPositon =  -1;
                if (fatalTag != null) {
                    org.htmlcleaner.HtmlCleaner.TagPos tagPos = cleanTimeValues._openTags.findTag(fatalTag);
                    if (tagPos != null) {
                        fatalTagPositon = tagPos.position;
                    }
                }
// iterates through the list of open tags from the end and check if there is some higher
                java.util.ListIterator<org.htmlcleaner.HtmlCleaner.TagPos> it = cleanTimeValues._openTags.list.listIterator(cleanTimeValues._openTags.list.size());
                while (it.hasPrevious()) {
                    org.htmlcleaner.HtmlCleaner.TagPos currTagPos = it.previous();
                    if (tag.isHigher(currTagPos.name)) {
                        return currTagPos.position <= fatalTagPositon;
                    }
                }
                return true;
            }
        }
        return false;
    }
    private org.htmlcleaner.TagNode createTagNode(org.htmlcleaner.TagNode startTagToken) {
        startTagToken.setFormed();
        return startTagToken;
    }
    private boolean isAllowedInLastOpenTag(org.htmlcleaner.BaseToken token, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        org.htmlcleaner.HtmlCleaner.TagPos last = cleanTimeValues._openTags.getLastTagPos();
        if (last != null) {
            if (last.info != null) {
                return last.info.allowsItem(token);
            }
        }
        return true;
    }
    private void saveToLastOpenTag(java.util.List nodeList, org.htmlcleaner.BaseToken tokenToAdd, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        org.htmlcleaner.HtmlCleaner.TagPos last = cleanTimeValues._openTags.getLastTagPos();
        if (last != null && last.info != null && last.info.isIgnorePermitted()) {
            return;
        }
        org.htmlcleaner.HtmlCleaner.TagPos rubbishPos = cleanTimeValues._openTags.findTagToPlaceRubbish();
        if (rubbishPos != null) {
            org.htmlcleaner.TagNode startTagToken = (org.htmlcleaner.TagNode) (nodeList.get(rubbishPos.position)) ;
            startTagToken.addItemForMoving(tokenToAdd);
        }
    }
    private boolean isStartToken(java.lang.Object o) {
        return (o instanceof org.htmlcleaner.TagNode) &&  !((org.htmlcleaner.TagNode) (o) ).isFormed();
    }
    void makeTree(java.util.List<org.htmlcleaner.BaseToken> nodeList, java.util.ListIterator<org.htmlcleaner.BaseToken> nodeIterator, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
// process while not reach the end of the list
        while (nodeIterator.hasNext()) {
            org.htmlcleaner.BaseToken token = nodeIterator.next();
            if (token instanceof org.htmlcleaner.EndTagToken) {
                org.htmlcleaner.EndTagToken endTagToken = (org.htmlcleaner.EndTagToken) (token) ;
                java.lang.String tagName = endTagToken.getName();
                org.htmlcleaner.TagInfo tag = tagInfoProvider.getTagInfo(tagName);
                if ((tag == null && properties.omitUnknownTags) || (tag != null && tag.isDeprecated() && properties.omitDeprecatedTags)) {
                    nodeIterator.set(null);
                }
                else if (tag != null &&  !tag.allowsBody()) {
                    nodeIterator.set(null);
                }
                else {
                    org.htmlcleaner.HtmlCleaner.TagPos matchingPosition = cleanTimeValues._openTags.findTag(tagName);
                    if (matchingPosition != null) {
                        java.util.List closed = this.closeSnippet(nodeList,matchingPosition,endTagToken,cleanTimeValues);
                        nodeIterator.set(null);
                        for (int i = closed.size() - 1; i >= 1; i--) {
                            org.htmlcleaner.TagNode closedTag = (org.htmlcleaner.TagNode) (closed.get(i)) ;
                            if (tag != null && tag.isContinueAfter(closedTag.getName())) {
                                nodeIterator.add(this.makeTagNodeCopy(closedTag,cleanTimeValues));
                                nodeIterator.previous();
                            }
                        }
                    }
                    else if ( !this.isAllowedInLastOpenTag(token,cleanTimeValues)) {
                        this.saveToLastOpenTag(nodeList,token,cleanTimeValues);
                        nodeIterator.set(null);
                    }
                }
            }
            else if (this.isStartToken(token)) {
                org.htmlcleaner.TagNode startTagToken = (org.htmlcleaner.TagNode) (token) ;
                java.lang.String tagName = startTagToken.getName();
                org.htmlcleaner.TagInfo tag = tagInfoProvider.getTagInfo(tagName);
                org.htmlcleaner.HtmlCleaner.TagPos lastTagPos = cleanTimeValues._openTags.isEmpty() ? null : cleanTimeValues._openTags.getLastTagPos();
                org.htmlcleaner.TagInfo lastTagInfo = lastTagPos == null ? null : tagInfoProvider.getTagInfo(lastTagPos.name);
                cleanTimeValues.allTags.add(tagName);
// add tag to set of all tags
// HTML open tag
                if ("html".equals(tagName)) {
                    this.addAttributesToTag(cleanTimeValues.htmlNode,startTagToken.getAttributes());
                    nodeIterator.set(null);
                }
                else if ("body".equals(tagName)) {
                    cleanTimeValues._bodyOpened = true;
                    this.addAttributesToTag(cleanTimeValues.bodyNode,startTagToken.getAttributes());
                    nodeIterator.set(null);
                }
                else if ("head".equals(tagName)) {
                    cleanTimeValues._headOpened = true;
                    this.addAttributesToTag(cleanTimeValues.headNode,startTagToken.getAttributes());
                    nodeIterator.set(null);
                }
                else if ((tag == null && properties.omitUnknownTags) || (tag != null && tag.isDeprecated() && properties.omitDeprecatedTags)) {
                    nodeIterator.set(null);
                }
                else if (tag == null && lastTagInfo != null &&  !lastTagInfo.allowsAnything()) {
                    this.saveToLastOpenTag(nodeList,token,cleanTimeValues);
                    nodeIterator.set(null);
                }
                else if (tag != null && tag.hasPermittedTags() && cleanTimeValues._openTags.someAlreadyOpen(tag.getPermittedTags())) {
                    nodeIterator.set(null);
                }
                else if (tag != null && tag.isUnique() && cleanTimeValues._openTags.tagEncountered(tagName)) {
                    nodeIterator.set(null);
                }
                else if ( !this.isFatalTagSatisfied(tag,cleanTimeValues)) {
                    nodeIterator.set(null);
                }
                else if (this.mustAddRequiredParent(tag,cleanTimeValues)) {
                    java.lang.String requiredParent = tag.getRequiredParent();
                    org.htmlcleaner.TagNode requiredParentStartToken = this.createTagNode(requiredParent,cleanTimeValues);
                    nodeIterator.previous();
                    nodeIterator.add(requiredParentStartToken);
                    nodeIterator.previous();
                }
                else if (tag != null && lastTagPos != null && tag.isMustCloseTag(lastTagInfo)) {
                    java.util.List closed = this.closeSnippet(nodeList,lastTagPos,startTagToken,cleanTimeValues);
                    int closedCount = closed.size();
// it is needed to copy some tags again in front of current, if there are any
                    if (tag.hasCopyTags() && closedCount > 0) {
// first iterates over list from the back and collects all start tokens
// in sequence that must be copied
                        java.util.ListIterator closedIt = closed.listIterator(closedCount);
                        java.util.List toBeCopied = new java.util.ArrayList();
                        while (closedIt.hasPrevious()) {
                            org.htmlcleaner.TagNode currStartToken = (org.htmlcleaner.TagNode) (closedIt.previous()) ;
                            if (tag.isCopy(currStartToken.getName())) {
                                toBeCopied.add(0,currStartToken);
                            }
                            else {
                                break;
                            }
                        }
                        if (toBeCopied.size() > 0) {
                            java.util.Iterator copyIt = toBeCopied.iterator();
                            while (copyIt.hasNext()) {
                                org.htmlcleaner.TagNode currStartToken = (org.htmlcleaner.TagNode) (copyIt.next()) ;
                                nodeIterator.add(this.makeTagNodeCopy(currStartToken,cleanTimeValues));
                            }
                            for (int i = 0; i < toBeCopied.size(); i++) {
                                nodeIterator.previous();
                            }
                        }
                    }
                    nodeIterator.previous();
                }
                else if ( !this.isAllowedInLastOpenTag(token,cleanTimeValues)) {
                    this.saveToLastOpenTag(nodeList,token,cleanTimeValues);
                    nodeIterator.set(null);
                }
                else if (tag != null &&  !tag.allowsBody()) {
                    org.htmlcleaner.TagNode newTagNode = this.createTagNode(startTagToken);
                    this.addPossibleHeadCandidate(tag,newTagNode,cleanTimeValues);
                    nodeIterator.set(newTagNode);
                }
                else {
                    cleanTimeValues._openTags.addTag(tagName,nodeIterator.previousIndex());
                }
            }
            else {
                if ( !this.isAllowedInLastOpenTag(token,cleanTimeValues)) {
                    this.saveToLastOpenTag(nodeList,token,cleanTimeValues);
                    nodeIterator.set(null);
                }
            }
        }
    }
    private void createDocumentNodes(java.util.List listNodes, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        java.util.Iterator it = listNodes.iterator();
        while (it.hasNext()) {
            java.lang.Object child = it.next();
            if (child == null) {
                continue;
            }
            boolean toAdd = true;
            if (child instanceof org.htmlcleaner.TagNode) {
                org.htmlcleaner.TagNode node = (org.htmlcleaner.TagNode) (child) ;
                org.htmlcleaner.TagInfo tag = tagInfoProvider.getTagInfo(node.getName());
                this.addPossibleHeadCandidate(tag,node,cleanTimeValues);
            }
            else {
                if (child instanceof org.htmlcleaner.ContentNode) {
                    toAdd =  !"".equals(child.toString());
                }
            }
            if (toAdd) {
                cleanTimeValues.bodyNode.addChild(child);
            }
        }
// move all viable head candidates to head section of the tree
        java.util.Iterator headIterator = cleanTimeValues._headTags.iterator();
        while (headIterator.hasNext()) {
            org.htmlcleaner.TagNode headCandidateNode = (org.htmlcleaner.TagNode) (headIterator.next()) ;
// check if this node is already inside a candidate for moving to head
            org.htmlcleaner.TagNode parent = headCandidateNode.getParent();
            boolean toMove = true;
            while (parent != null) {
                if (cleanTimeValues._headTags.contains(parent)) {
                    toMove = false;
                    break;
                }
                parent = parent.getParent();
            }
            if (toMove) {
                headCandidateNode.removeFromTree();
                cleanTimeValues.headNode.addChild(headCandidateNode);
            }
        }
    }
    private java.util.List closeSnippet(java.util.List nodeList, org.htmlcleaner.HtmlCleaner.TagPos tagPos, java.lang.Object toNode, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        java.util.List closed = new java.util.ArrayList();
        java.util.ListIterator it = nodeList.listIterator(tagPos.position);
        org.htmlcleaner.TagNode tagNode = null;
        java.lang.Object item = it.next();
        boolean isListEnd = false;
        while ((toNode == null &&  !isListEnd) || (toNode != null && item != toNode)) {
            if (this.isStartToken(item)) {
                org.htmlcleaner.TagNode startTagToken = (org.htmlcleaner.TagNode) (item) ;
                closed.add(startTagToken);
                java.util.List<org.htmlcleaner.BaseToken> itemsToMove = startTagToken.getItemsToMove();
                if (itemsToMove != null) {
                    org.htmlcleaner.HtmlCleaner.OpenTags prevOpenTags = cleanTimeValues._openTags;
                    cleanTimeValues._openTags = new org.htmlcleaner.HtmlCleaner.OpenTags();
                    this.makeTree(itemsToMove,itemsToMove.listIterator(0),cleanTimeValues);
                    this.closeAll(itemsToMove,cleanTimeValues);
                    startTagToken.setItemsToMove(null);
                    cleanTimeValues._openTags = prevOpenTags;
                }
                org.htmlcleaner.TagNode newTagNode = this.createTagNode(startTagToken);
                org.htmlcleaner.TagInfo tag = tagInfoProvider.getTagInfo(newTagNode.getName());
                this.addPossibleHeadCandidate(tag,newTagNode,cleanTimeValues);
                if (tagNode != null) {
                    tagNode.addChildren(itemsToMove);
                    tagNode.addChild(newTagNode);
                    it.set(null);
                }
                else {
                    if (itemsToMove != null) {
                        itemsToMove.add(newTagNode);
                        it.set(itemsToMove);
                    }
                    else {
                        it.set(newTagNode);
                    }
                }
                cleanTimeValues._openTags.removeTag(newTagNode.getName());
                tagNode = newTagNode;
            }
            else {
                if (tagNode != null) {
                    it.set(null);
                    if (item != null) {
                        tagNode.addChild(item);
                    }
                }
            }
            if (it.hasNext()) {
                item = it.next();
            }
            else {
                isListEnd = true;
            }
        }
        return closed;
    }
/**
     * Close all unclosed tags if there are any.
     */
    private void closeAll(java.util.List<org.htmlcleaner.BaseToken> nodeList, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        org.htmlcleaner.HtmlCleaner.TagPos firstTagPos = cleanTimeValues._openTags.findFirstTagPos();
        if (firstTagPos != null) {
            this.closeSnippet(nodeList,firstTagPos,null,cleanTimeValues);
        }
    }
/**
     * Checks if specified tag with specified info is candidate for moving to head section.
     * @param tagInfo
     * @param tagNode
     */
    private void addPossibleHeadCandidate(org.htmlcleaner.TagInfo tagInfo, org.htmlcleaner.TagNode tagNode, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        if (tagInfo != null && tagNode != null) {
            if (tagInfo.isHeadTag() || (tagInfo.isHeadAndBodyTag() && cleanTimeValues._headOpened &&  !cleanTimeValues._bodyOpened)) {
                cleanTimeValues._headTags.add(tagNode);
            }
        }
    }
    public org.htmlcleaner.CleanerProperties getProperties() {
        return properties;
    }
    private void setPruneTags(java.lang.String pruneTags, org.htmlcleaner.HtmlCleaner.CleanTimeValues cleanTimeValues) {
        cleanTimeValues.pruneTagSet.clear();
        cleanTimeValues.pruneNodeSet.clear();
        if (pruneTags != null) {
            java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(pruneTags, ",");
            while (tokenizer.hasMoreTokens()) {
                cleanTimeValues.pruneTagSet.add(tokenizer.nextToken().trim().toLowerCase());
            }
        }
    }
/**
     * @return ITagInfoProvider instance for this HtmlCleaner
     */
    public org.htmlcleaner.ITagInfoProvider getTagInfoProvider() {
        return tagInfoProvider;
    }
/**
     * @return Transormations defined for this instance of cleaner
     */
    public org.htmlcleaner.CleanerTransformations getTransformations() {
        return transformations;
    }
/**
     * Sets tranformations for this cleaner instance.
     * @param transformations
     */
    public void setTransformations(org.htmlcleaner.CleanerTransformations transformations) {
        this.transformations = transformations;
    }
/**
     * For the specified node, returns it's content as string.
     * @param node
     */
    public java.lang.String getInnerHtml(org.htmlcleaner.TagNode node) {
        if (node != null) {
            try {
                java.lang.String content = new org.htmlcleaner.SimpleXmlSerializer(properties).getAsString(node);
                int index1 = content.indexOf("<" + node.getName());
                index1 = content.indexOf('\u003e',index1 + 1);
                int index2 = content.lastIndexOf('\u003c');
                return index1 >= 0 && index1 <= index2 ? content.substring(index1 + 1,index2) : null;
            }
            catch (java.io.IOException e){
                throw new org.htmlcleaner.HtmlCleanerException(e);
            }
        }
        else {
            throw new org.htmlcleaner.HtmlCleanerException("Cannot return inner html of the null node!");
        }
    }
/**
     * For the specified tag node, defines it's html content. This causes cleaner to
     * reclean given html portion and insert it inside the node instead of previous content.
     * @param node
     * @param content
     */
    public void setInnerHtml(org.htmlcleaner.TagNode node, java.lang.String content) {
        if (node != null) {
            java.lang.String nodeName = node.getName();
            java.lang.StringBuilder html = new java.lang.StringBuilder();
            html.append("<" + nodeName + " marker=''>");
            html.append(content);
            html.append("</" + nodeName + ">");
            org.htmlcleaner.TagNode parent = node.getParent();
            while (parent != null) {
                java.lang.String parentName = parent.getName();
                html.insert(0,"<" + parentName + ">");
                html.append("</" + parentName + ">");
                parent = parent.getParent();
            }
            org.htmlcleaner.TagNode rootNode = this.clean(html.toString());
            org.htmlcleaner.TagNode cleanedNode = rootNode.findElementHavingAttribute("marker",true);
            if (cleanedNode != null) {
                node.setChildren(cleanedNode.getChildren());
            }
        }
    }
}
