/**
 * Defines action to be performed on TagNodes
 */
package org.htmlcleaner;
import org.htmlcleaner.*;
abstract public interface TagNodeVisitor {
/**
     * Action to be performed on single node in the tree
     * @param parentNode Parent of tagNode
     * @param htmlNode node visited
     * @return True if tree traversal should be continued, false if it has to stop.
     */
    abstract public boolean visit(org.htmlcleaner.TagNode parentNode, org.htmlcleaner.HtmlNode htmlNode);
}
