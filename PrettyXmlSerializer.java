/*  Copyright (c) 2006-2007, Vladimir Nikic
    All rights reserved.

    Redistribution and use of this software in source and binary forms,
    with or without modification, are permitted provided that the following
    conditions are met:

    * Redistributions of source code must retain the above
      copyright notice, this list of conditions and the
      following disclaimer.

    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the
      following disclaimer in the documentation and/or other
      materials provided with the distribution.

    * The name of HtmlCleaner may not be used to endorse or promote
      products derived from this software without specific prior
      written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    You can contact Vladimir Nikic by sending e-mail to
    nikic_vladimir@yahoo.com. Please include the word "HtmlCleaner" in the
    subject line.
*/
/**
 * <p>Pretty XML serializer - creates resulting XML with indenting lines.</p>
 */
package org.htmlcleaner;
import org.htmlcleaner.*;
import java.io.IOException;
import java.io.Writer;
import java.util.*;
public class PrettyXmlSerializer extends org.htmlcleaner.XmlSerializer {
    final private static java.lang.String DEFAULT_INDENTATION_STRING = "	";
    private java.lang.String indentString = DEFAULT_INDENTATION_STRING;
    private java.util.List<java.lang.String> indents = new java.util.ArrayList<java.lang.String>();
    public PrettyXmlSerializer(org.htmlcleaner.CleanerProperties props) {
        this(props,DEFAULT_INDENTATION_STRING);
    }
    public PrettyXmlSerializer(org.htmlcleaner.CleanerProperties props, java.lang.String indentString) {
        super(props);
        this.indentString = indentString;
    }
    protected void serialize(org.htmlcleaner.TagNode tagNode, java.io.Writer writer) throws java.io.IOException {
        this.serializePrettyXml(tagNode,writer,0);
    }
/**
	 * @param level
	 * @return Appropriate indentation for the specified depth.
	 */
    private synchronized java.lang.String getIndent(int level) {
        int size = indents.size();
        if (size <= level) {
            java.lang.String prevIndent = size == 0 ? null : indents.get(size - 1);
            for (int i = size; i <= level; i++) {
                java.lang.String currIndent = prevIndent == null ? "" : prevIndent + indentString;
                indents.add(currIndent);
                prevIndent = currIndent;
            }
        }
        return indents.get(level);
    }
    private java.lang.String getIndentedText(java.lang.String content, int level) {
        java.lang.String indent = this.getIndent(level);
        java.lang.StringBuilder result = new java.lang.StringBuilder(content.length());
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(content, "\n\r");
        while (tokenizer.hasMoreTokens()) {
            java.lang.String line = tokenizer.nextToken().trim();
            if ( !"".equals(line)) {
                result.append(indent).append(line).append("\n");
            }
        }
        return result.toString();
    }
    private java.lang.String getSingleLineOfChildren(java.util.List children) {
        java.lang.StringBuilder result = new java.lang.StringBuilder();
        java.util.Iterator childrenIt = children.iterator();
        boolean isFirst = true;
        while (childrenIt.hasNext()) {
            java.lang.Object child = childrenIt.next();
            if ( !(child instanceof org.htmlcleaner.ContentNode)) {
                return null;
            }
            else {
                java.lang.String content = child.toString();
// if first item trims it from left
                if (isFirst) {
                    content = org.htmlcleaner.Utils.ltrim(content);
                }
// if last item trims it from right
                if ( !childrenIt.hasNext()) {
                    content = org.htmlcleaner.Utils.rtrim(content);
                }
                if (content.indexOf("\n") >= 0 || content.indexOf("\r") >= 0) {
                    return null;
                }
                result.append(content);
            }
            isFirst = false;
        }
        return result.toString();
    }
    protected void serializePrettyXml(org.htmlcleaner.TagNode tagNode, java.io.Writer writer, int level) throws java.io.IOException {
        java.util.List tagChildren = tagNode.getChildren();
        boolean isHeadlessNode = org.htmlcleaner.Utils.isEmptyString(tagNode.getName());
        java.lang.String indent = isHeadlessNode ? "" : this.getIndent(level);
        writer.write(indent);
        this.serializeOpenTag(tagNode,writer,true);
        if ( !this.isMinimizedTagSyntax(tagNode)) {
            java.lang.String singleLine = this.getSingleLineOfChildren(tagChildren);
            boolean dontEscape = this.dontEscape(tagNode);
            if (singleLine != null) {
                if ( !this.dontEscape(tagNode)) {
                    writer.write(this.escapeXml(singleLine));
                }
                else {
                    writer.write(singleLine.replaceAll("]]>","]]&gt;"));
                }
            }
            else {
                if ( !isHeadlessNode) {
                    writer.write("\n");
                }
                for (java.lang.Object child : tagChildren){
                    if (child instanceof org.htmlcleaner.TagNode) {
                        this.serializePrettyXml((org.htmlcleaner.TagNode) (child) ,writer,isHeadlessNode ? level : level + 1);
                    }
                    else if (child instanceof org.htmlcleaner.ContentNode) {
                        java.lang.String content = dontEscape ? child.toString().replaceAll("]]>","]]&gt;") : this.escapeXml(child.toString());
                        writer.write(this.getIndentedText(content,isHeadlessNode ? level : level + 1));
                    }
                    else if (child instanceof org.htmlcleaner.CommentNode) {
                        org.htmlcleaner.CommentNode commentNode = (org.htmlcleaner.CommentNode) (child) ;
                        java.lang.String content = commentNode.getCommentedContent();
                        writer.write(this.getIndentedText(content,isHeadlessNode ? level : level + 1));
                    }
                }
;
            }
            if (singleLine == null) {
                writer.write(indent);
            }
            this.serializeEndTag(tagNode,writer,true);
        }
    }
}
