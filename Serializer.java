/*  Copyright (c) 2006-2007, Vladimir Nikic
    All rights reserved.
	
    Redistribution and use of this software in source and binary forms, 
    with or without modification, are permitted provided that the following 
    conditions are met:
	
    * Redistributions of source code must retain the above
      copyright notice, this list of conditions and the
      following disclaimer.
	
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the
      following disclaimer in the documentation and/or other
      materials provided with the distribution.
	
    * The name of HtmlCleaner may not be used to endorse or promote 
      products derived from this software without specific prior
      written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
    POSSIBILITY OF SUCH DAMAGE.
	
    You can contact Vladimir Nikic by sending e-mail to
    nikic_vladimir@yahoo.com. Please include the word "HtmlCleaner" in the
    subject line.
*/
/**
 * <p>Basic abstract serializer - contains common logic for descendants (methods <code>writeXXX()</code>.</p>
 */
package org.htmlcleaner;
import org.htmlcleaner.*;
import java.io.*;
import java.util.*;
abstract public class Serializer extends java.lang.Object {
/**
     * Used to implement serialization with missing envelope - omiting open and close tags, just
     * serialize children.
     */
    private class HeadlessTagNode extends org.htmlcleaner.TagNode {
        public HeadlessTagNode(org.htmlcleaner.TagNode wrappedNode) {
            super("");
            this.getAttributes().putAll(wrappedNode.getAttributes());
            this.getChildren().addAll(wrappedNode.getChildren());
            this.setDocType(wrappedNode.getDocType());
            java.util.Map<java.lang.String, java.lang.String> nsDecls = this.getNamespaceDeclarations();
            if (nsDecls != null) {
                java.util.Map<java.lang.String, java.lang.String> wrappedNSDecls = wrappedNode.getNamespaceDeclarations();
                if (wrappedNSDecls != null) {
                    nsDecls.putAll(wrappedNSDecls);
                }
            }
        }
    }
    protected org.htmlcleaner.CleanerProperties props;
    public Serializer(org.htmlcleaner.CleanerProperties props) {
        super();
        this.props = props;
    }
/**
     * Writes specified TagNode to the output stream, using specified charset and optionally omits node envelope
     * (skips open and close tags of the node).
     * @param tagNode Node to be written
     * @param out Output stream
     * @param charset Charset of the output
     * @param omitEnvelope Tells whether to skip open and close tag of the node.
     * @throws IOException
     */
    public void writeToStream(org.htmlcleaner.TagNode tagNode, java.io.OutputStream out, java.lang.String charset, boolean omitEnvelope) throws java.io.IOException {
        this.write(tagNode,new java.io.OutputStreamWriter(out, charset),charset,omitEnvelope);
    }
/**
     * Writes specified TagNode to the output stream, using specified charset.
     * @param tagNode Node to be written
     * @param out Output stream
     * @param charset Charset of the output
     * @throws IOException
     */
    public void writeToStream(org.htmlcleaner.TagNode tagNode, java.io.OutputStream out, java.lang.String charset) throws java.io.IOException {
        this.writeToStream(tagNode,out,charset,false);
    }
/**
     * Writes specified TagNode to the output stream, using system default charset and optionally omits node envelope
     * (skips open and close tags of the node).
     * @param tagNode Node to be written
     * @param out Output stream
     * @param omitEnvelope Tells whether to skip open and close tag of the node.
     * @throws IOException
     */
    public void writeToStream(org.htmlcleaner.TagNode tagNode, java.io.OutputStream out, boolean omitEnvelope) throws java.io.IOException {
        this.writeToStream(tagNode,out,org.htmlcleaner.HtmlCleaner.DEFAULT_CHARSET,omitEnvelope);
    }
/**
     * Writes specified TagNode to the output stream, using system default charset.
     * @param tagNode Node to be written
     * @param out Output stream
     * @throws IOException
     */
    public void writeToStream(org.htmlcleaner.TagNode tagNode, java.io.OutputStream out) throws java.io.IOException {
        this.writeToStream(tagNode,out,false);
    }
/**
     * Writes specified TagNode to the file, using specified charset and optionally omits node envelope
     * (skips open and close tags of the node).
     * @param tagNode Node to be written
     * @param fileName Output file name
     * @param charset Charset of the output
     * @param omitEnvelope Tells whether to skip open and close tag of the node.
     * @throws IOException
     */
    public void writeToFile(org.htmlcleaner.TagNode tagNode, java.lang.String fileName, java.lang.String charset, boolean omitEnvelope) throws java.io.IOException {
        this.writeToStream(tagNode,new java.io.FileOutputStream(fileName),charset,omitEnvelope);
    }
/**
     * Writes specified TagNode to the file, using specified charset.
     * @param tagNode Node to be written
     * @param fileName Output file name
     * @param charset Charset of the output
     * @throws IOException
     */
    public void writeToFile(org.htmlcleaner.TagNode tagNode, java.lang.String fileName, java.lang.String charset) throws java.io.IOException {
        this.writeToFile(tagNode,fileName,charset,false);
    }
/**
     * Writes specified TagNode to the file, using specified charset and optionally omits node envelope
     * (skips open and close tags of the node).
     * @param tagNode Node to be written
     * @param fileName Output file name
     * @param omitEnvelope Tells whether to skip open and close tag of the node.
     * @throws IOException
     */
    public void writeToFile(org.htmlcleaner.TagNode tagNode, java.lang.String fileName, boolean omitEnvelope) throws java.io.IOException {
        this.writeToFile(tagNode,fileName,org.htmlcleaner.HtmlCleaner.DEFAULT_CHARSET,omitEnvelope);
    }
/**
     * Writes specified TagNode to the file, using system default charset.
     * @param tagNode Node to be written
     * @param fileName Output file name
     * @throws IOException
     */
    public void writeToFile(org.htmlcleaner.TagNode tagNode, java.lang.String fileName) throws java.io.IOException {
        this.writeToFile(tagNode,fileName,false);
    }
/**
     * @param tagNode Node to serialize to string
     * @param charset Charset of the output - stands in xml declaration part
     * @param omitEnvelope Tells whether to skip open and close tag of the node.
     * @return Output as string
     * @throws IOException
     */
    public java.lang.String getAsString(org.htmlcleaner.TagNode tagNode, java.lang.String charset, boolean omitEnvelope) throws java.io.IOException {
        java.io.StringWriter writer = new java.io.StringWriter();
        this.write(tagNode,writer,charset,omitEnvelope);
        return writer.getBuffer().toString();
    }
/**
     * @param tagNode Node to serialize to string
     * @param charset Charset of the output - stands in xml declaration part
     * @return Output as string
     * @throws IOException
     */
    public java.lang.String getAsString(org.htmlcleaner.TagNode tagNode, java.lang.String charset) throws java.io.IOException {
        return this.getAsString(tagNode,charset,false);
    }
/**
     * @param tagNode Node to serialize to string
     * @param omitEnvelope Tells whether to skip open and close tag of the node.
     * @return Output as string
     * @throws IOException
     */
    public java.lang.String getAsString(org.htmlcleaner.TagNode tagNode, boolean omitEnvelope) throws java.io.IOException {
        return this.getAsString(tagNode,org.htmlcleaner.HtmlCleaner.DEFAULT_CHARSET,omitEnvelope);
    }
/**
     * @param tagNode Node to serialize to string
     * @return Output as string
     * @throws IOException
     */
    public java.lang.String getAsString(org.htmlcleaner.TagNode tagNode) throws java.io.IOException {
        return this.getAsString(tagNode,false);
    }
/**
     * Writes specified node using specified writer.
     * @param tagNode Node to serialize.
     * @param writer Writer instance
     * @param charset Charset of the output
     * @throws IOException
     */
    public void write(org.htmlcleaner.TagNode tagNode, java.io.Writer writer, java.lang.String charset) throws java.io.IOException {
        this.write(tagNode,writer,charset,false);
    }
/**
     * Writes specified node using specified writer.
     * @param tagNode Node to serialize.
     * @param writer Writer instance
     * @param charset Charset of the output
     * @param omitEnvelope Tells whether to skip open and close tag of the node.
     * @throws IOException
     */
    public void write(org.htmlcleaner.TagNode tagNode, java.io.Writer writer, java.lang.String charset, boolean omitEnvelope) throws java.io.IOException {
        if (omitEnvelope) {
            tagNode = new org.htmlcleaner.Serializer.HeadlessTagNode(tagNode);
        }
        writer = new java.io.BufferedWriter(writer);
        if ( !props.isOmitXmlDeclaration()) {
            java.lang.String declaration = "<?xml version=\"1.0\"";
            if (charset != null) {
                declaration += " encoding=\"" + charset + "\"";
            }
            declaration += "?>";
            writer.write(declaration + "\n");
        }
        if ( !props.isOmitDoctypeDeclaration()) {
            org.htmlcleaner.DoctypeToken doctypeToken = tagNode.getDocType();
            if (doctypeToken != null) {
                doctypeToken.serialize(this,writer);
            }
        }
        this.serialize(tagNode,writer);
        writer.flush();
        writer.close();
    }
    protected boolean isScriptOrStyle(org.htmlcleaner.TagNode tagNode) {
        java.lang.String tagName = tagNode.getName();
        return "script".equalsIgnoreCase(tagName) || "style".equalsIgnoreCase(tagName);
    }
    abstract protected void serialize(org.htmlcleaner.TagNode tagNode, java.io.Writer writer) throws java.io.IOException;
}
