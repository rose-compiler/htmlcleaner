/*  Copyright (c) 2006-2007, Vladimir Nikic
    All rights reserved.

    Redistribution and use of this software in source and binary forms,
    with or without modification, are permitted provided that the following
    conditions are met:

    * Redistributions of source code must retain the above
      copyright notice, this list of conditions and the
      following disclaimer.

    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the
      following disclaimer in the documentation and/or other
      materials provided with the distribution.

    * The name of HtmlCleaner may not be used to endorse or promote
      products derived from this software without specific prior
      written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    You can contact Vladimir Nikic by sending e-mail to
    nikic_vladimir@yahoo.com. Please include the word "HtmlCleaner" in the
    subject line.
*/
/**
 * Default tag provider - reads XML file in specified format and creates tag infos
 */
package org.htmlcleaner;
import org.htmlcleaner.*;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.net.URL;
public class ConfigFileTagProvider extends java.util.HashMap implements org.htmlcleaner.ITagInfoProvider {
// obtaining instance of the SAX parser factory
// tells whether to generate code of the tag provider class based on XML configuration file
// to the standard output
/**
     * Generates code for tag provider class from specified configuration XML file.
     * In order to create custom tag info provider, make config file and call this main method
     * with the specified file. Output will be generated on the standard output. This way default
     * tag provider (class DefaultTagProvider) is generated from default.xml which which is packaged
     * in the source distribution.
     *
     * @param args
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
/**
    * SAX parser for tag configuration files.
    */
    private class ConfigParser extends org.xml.sax.helpers.DefaultHandler {
        private org.htmlcleaner.TagInfo tagInfo = null;
        private java.lang.String dependencyName = null;
        private java.util.Map tagInfoMap;
        public ConfigParser(java.util.Map tagInfoMap) {
            super();
            this.tagInfoMap = tagInfoMap;
        }
        public void parse(org.xml.sax.InputSource in) throws java.io.IOException, org.xml.sax.SAXException, javax.xml.parsers.ParserConfigurationException {
            javax.xml.parsers.SAXParser parser = parserFactory.newSAXParser();
            parser.parse(in,this);
        }
        public void characters(char[] ch, int start, int length) throws org.xml.sax.SAXException {
            if (tagInfo != null) {
                java.lang.String value = new java.lang.String(ch, start, length).trim();
                if ("fatal-tags".equals(dependencyName)) {
                    tagInfo.defineFatalTags(value);
                    if (generateCode) {
                        java.lang.System.out.println("tagInfo.defineFatalTags(\"" + value + "\");");
                    }
                }
                else if ("req-enclosing-tags".equals(dependencyName)) {
                    tagInfo.defineRequiredEnclosingTags(value);
                    if (generateCode) {
                        java.lang.System.out.println("tagInfo.defineRequiredEnclosingTags(\"" + value + "\");");
                    }
                }
                else if ("forbidden-tags".equals(dependencyName)) {
                    tagInfo.defineForbiddenTags(value);
                    if (generateCode) {
                        java.lang.System.out.println("tagInfo.defineForbiddenTags(\"" + value + "\");");
                    }
                }
                else if ("allowed-children-tags".equals(dependencyName)) {
                    tagInfo.defineAllowedChildrenTags(value);
                    if (generateCode) {
                        java.lang.System.out.println("tagInfo.defineAllowedChildrenTags(\"" + value + "\");");
                    }
                }
                else if ("higher-level-tags".equals(dependencyName)) {
                    tagInfo.defineHigherLevelTags(value);
                    if (generateCode) {
                        java.lang.System.out.println("tagInfo.defineHigherLevelTags(\"" + value + "\");");
                    }
                }
                else if ("close-before-copy-inside-tags".equals(dependencyName)) {
                    tagInfo.defineCloseBeforeCopyInsideTags(value);
                    if (generateCode) {
                        java.lang.System.out.println("tagInfo.defineCloseBeforeCopyInsideTags(\"" + value + "\");");
                    }
                }
                else if ("close-inside-copy-after-tags".equals(dependencyName)) {
                    tagInfo.defineCloseInsideCopyAfterTags(value);
                    if (generateCode) {
                        java.lang.System.out.println("tagInfo.defineCloseInsideCopyAfterTags(\"" + value + "\");");
                    }
                }
                else if ("close-before-tags".equals(dependencyName)) {
                    tagInfo.defineCloseBeforeTags(value);
                    if (generateCode) {
                        java.lang.System.out.println("tagInfo.defineCloseBeforeTags(\"" + value + "\");");
                    }
                }
            }
        }
        public void startElement(java.lang.String uri, java.lang.String localName, java.lang.String qName, org.xml.sax.Attributes attributes) throws org.xml.sax.SAXException {
            if ("tag".equals(qName)) {
                java.lang.String name = attributes.getValue("name");
                java.lang.String content = attributes.getValue("content");
                java.lang.String section = attributes.getValue("section");
                java.lang.String deprecated = attributes.getValue("deprecated");
                java.lang.String unique = attributes.getValue("unique");
                java.lang.String ignorePermitted = attributes.getValue("ignore-permitted");
                tagInfo = new org.htmlcleaner.TagInfo(name, "all".equals(content) ? org.htmlcleaner.TagInfo.CONTENT_ALL : ("none".equals(content) ? org.htmlcleaner.TagInfo.CONTENT_NONE : org.htmlcleaner.TagInfo.CONTENT_TEXT), "all".equals(section) ? org.htmlcleaner.TagInfo.HEAD_AND_BODY : ("head".equals(section) ? org.htmlcleaner.TagInfo.HEAD : org.htmlcleaner.TagInfo.BODY), deprecated != null && "true".equals(deprecated), unique != null && "true".equals(unique), ignorePermitted != null && "true".equals(ignorePermitted));
                if (generateCode) {
                    java.lang.String s = "tagInfo = new TagInfo(\"#1\", #2, #3, #4, #5, #6);";
                    s = s.replaceAll("#1",name);
                    s = s.replaceAll("#2","all".equals(content) ? "TagInfo.CONTENT_ALL" : ("none".equals(content) ? "TagInfo.CONTENT_NONE" : " TagInfo.CONTENT_TEXT"));
                    s = s.replaceAll("#3","all".equals(section) ? "TagInfo.HEAD_AND_BODY" : ("head".equals(section) ? "TagInfo.HEAD" : "TagInfo.BODY"));
                    s = s.replaceAll("#4",java.lang.Boolean.toString(deprecated != null && "true".equals(deprecated)));
                    s = s.replaceAll("#5",java.lang.Boolean.toString(unique != null && "true".equals(unique)));
                    s = s.replaceAll("#6",java.lang.Boolean.toString(ignorePermitted != null && "true".equals(ignorePermitted)));
                    java.lang.System.out.println(s);
                }
            }
            else if ( !"tags".equals(qName)) {
                dependencyName = qName;
            }
        }
        public void endElement(java.lang.String uri, java.lang.String localName, java.lang.String qName) throws org.xml.sax.SAXException {
            if ("tag".equals(qName)) {
                if (tagInfo != null) {
                    tagInfoMap.put(tagInfo.getName(),tagInfo);
                    if (generateCode) {
                        java.lang.System.out.println("this.put(\"" + tagInfo.getName() + "\", tagInfo);\n");
                    }
                }
                tagInfo = null;
            }
            else if ( !"tags".equals(qName)) {
                dependencyName = null;
            }
        }
    }
    static javax.xml.parsers.SAXParserFactory parserFactory = javax.xml.parsers.SAXParserFactory.newInstance();
    static {
        parserFactory.setValidating(false);
        parserFactory.setNamespaceAware(false);
    }
    private boolean generateCode = false;
    public ConfigFileTagProvider() {
        super();
    }
    public ConfigFileTagProvider(org.xml.sax.InputSource inputSource) {
        super();
        try {
            new org.htmlcleaner.ConfigFileTagProvider.ConfigParser(this).parse(inputSource);
        }
        catch (java.lang.Exception e){
            throw new org.htmlcleaner.HtmlCleanerException("Error parsing tag configuration file!", e);
        }
    }
    public ConfigFileTagProvider(java.io.File file) {
        super();
        try {
            new org.htmlcleaner.ConfigFileTagProvider.ConfigParser(this).parse(new org.xml.sax.InputSource(new java.io.FileReader(file)));
        }
        catch (java.lang.Exception e){
            throw new org.htmlcleaner.HtmlCleanerException("Error parsing tag configuration file!", e);
        }
    }
    public ConfigFileTagProvider(java.net.URL url) {
        super();
        try {
            java.lang.Object content = url.getContent();
            if (content instanceof java.io.InputStream) {
                java.io.InputStreamReader reader = new java.io.InputStreamReader((java.io.InputStream) (content) );
                new org.htmlcleaner.ConfigFileTagProvider.ConfigParser(this).parse(new org.xml.sax.InputSource(reader));
            }
        }
        catch (java.lang.Exception e){
            throw new org.htmlcleaner.HtmlCleanerException("Error parsing tag configuration file!", e);
        }
    }
    public org.htmlcleaner.TagInfo getTagInfo(java.lang.String tagName) {
        return (org.htmlcleaner.TagInfo) (this.get(tagName)) ;
    }
    public static void main(java.lang.String[] args) throws javax.xml.parsers.ParserConfigurationException, org.xml.sax.SAXException, java.io.IOException {
        final org.htmlcleaner.ConfigFileTagProvider provider = new org.htmlcleaner.ConfigFileTagProvider();
        provider.generateCode = true;
        java.io.File configFile = new java.io.File("default.xml");
        java.lang.String packagePath = "org.htmlcleaner";
        java.lang.String className = "DefaultTagProvider";
        final org.htmlcleaner.ConfigFileTagProvider.ConfigParser parser = provider.new ConfigParser(provider);
        java.lang.System.out.println("package " + packagePath + ";");
        java.lang.System.out.println("import java.util.HashMap;");
        java.lang.System.out.println("public class " + className + " extends HashMap implements ITagInfoProvider {");
        java.lang.System.out.println("public " + className + "() {");
        java.lang.System.out.println("TagInfo tagInfo;");
        parser.parse(new org.xml.sax.InputSource(new java.io.FileReader(configFile)));
        java.lang.System.out.println("}");
        java.lang.System.out.println("}");
    }
}
